/*
 * Copyright (C) 2015 Nicola De Maio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package scotti.distributions;

import beast.base.core.Citation;
import beast.base.core.Description;
import beast.base.core.Input;
import beast.base.core.Input.Validate;
import beast.base.core.Loggable;
import beast.base.evolution.tree.Node;
import beast.base.evolution.tree.Tree;
import beast.base.inference.parameter.RealParameter;
import beast.base.util.Randomizer;
import com.google.common.collect.Lists;
import scotti.evolution.tree.*;

import java.io.PrintStream;
import java.util.*;

/**
 *
 * @author Nicola De Maio
 */
@Citation("Structured COalescent Transmission Tree Inference (SCOTTI). De Maio N., Wu C., Wilson D., PLOS Computational Biology 2016")
@Description("Likelihood of Coalescent tree (Volz-type) under the Volz approximation to the structured coalescent, fast version under the assumption that all demes have the same popsize and migration rates.")
public class StructuredCoalescentTreeDensityNew extends MultiTypeTreeDistributionConcise implements Loggable {

	public Input<MigrationModelUniform> migrationModelInput = new Input<MigrationModelUniform>(
            "migrationModelUniform", "Model of migration between demes.",
            Validate.REQUIRED);
    public Input<Boolean> checkValidityInput = new Input<Boolean>(
            "checkValidity", "Explicitly check validity of colouring.  "
            +"(Default false.)  "
            +"Useful if operators are in danger of proposing invalid trees.",
            false);
    public Input<Boolean> limitedLifespanInput = new Input<Boolean>(
            "limitedLifespan", "if true, non-sampled demes have only limited lifespan, otherwise they last forever.  "
            +"(Default true.)  ",
            true);
    public Input<Boolean> penalizeMigrationInput = new Input<Boolean>(
            "penalizeMigration", "if true, penalize lineages that are in a deme at its closure, otherwise don't (default and original version).  ",
            false);
    
    protected MigrationModelUniform migrationModelUniform;
    protected MultiTypeTreeConcise mtTree;
    protected MultiTypeTreeConcise storedMtTree;
    protected boolean checkValidity;
    boolean limitedLifespan;
    boolean penalizeMigration;
    final boolean sampleMig=true;//false
    public conciseVector[] pLikes = null;
    public conciseVector[] storedPLikes = null;
    public ArrayList<conciseVector>[] pLikesNew = null;
    public ArrayList<Double>[] pDates = null;
    public ArrayList<conciseVector>[] storedPLikesNew = null;
    public ArrayList<Double>[] storedPDates = null;
    //public ArrayList<Integer> numberList = null;
    
    double meanLife=0.0, latestDeath=1000000.0, earliestBirth=-1000000.0;
    //int extraDemes ;
    demeEvent[] demeEvents;
    
    private enum SCEventKind {
        COALESCE, SAMPLE, ADD_DEME, REMOVE_DEME
    };

    private class SCEvent {
        double time;
        int type;
        SCEventKind kind;
        Node node;
    }
    private List<SCEvent> eventList;
    //private List<SCEvent> storedEventList;
    private List<conciseVector> lineageCountList;
    private List<conciseVector> lineageCountSquareList;
    private List<conciseVector> lineageProbsList1;
    private List<conciseVector> lineageProbsList2;
    private List<conciseVector> lineageCountListAfter;
    private List<conciseVector> lineageCountSquareListAfter;
    //private List<conciseVector> storedLineageProbsList1;
    
    public Integer[] countList;
    public String[] demeList;
    
    public class MigEvent {
    	public int son;
    	public int parent;
    	public double time;
    }
    
    public class demeEvent {
    	  public Integer index;
    	  public Double time;
    	  public String type;

    	  public demeEvent(Integer left, Double right, String S) {
    	    this.index = left;
    	    this.time = right;
    	    this.type = S;
    	  }
    }
    //public int nDemes;
    
    public List<demeEvent> demeEventsList;
    
    public void init(PrintStream out) throws IllegalArgumentException {
    	((Tree) mtTree).init(out);
    }
    
    @Override
    public void log(final long nSample, final PrintStream out) {
    	setAllDemes();
        out.print("tree STATE_" + nSample + " = ");
        out.print(toSortedNewickConcise(mtTree.getRoot(), new int[1],demeList,countList)+";");
    }
    
    public String toSortedNewickConcise(Node node, int[] iMaxNodeInClade, String[] demeList, Integer[] countList) {
        StringBuilder buf = new StringBuilder();
        if (node.getLeft() != null) {
            buf.append("(");
            String sChild1 = toSortedNewickConcise(node.getLeft(), iMaxNodeInClade, demeList, countList);
            int iChild1 = iMaxNodeInClade[0];
            if (node.getRight() != null) {
                String sChild2 = toSortedNewickConcise(node.getRight(), iMaxNodeInClade, demeList, countList);
                int iChild2 = iMaxNodeInClade[0];
                if (iChild1 > iChild2) {
                    buf.append(sChild2);
                    buf.append(",");
                    buf.append(sChild1);
                } else {
                    buf.append(sChild1);
                    buf.append(",");
                    buf.append(sChild2);
                    iMaxNodeInClade[0] = iChild1;
                }
            } else {
                buf.append(sChild1);
            }
            buf.append(")");
            if (getID() != null) {
                buf.append(node.getNr()+1);
            }
        } else {
            iMaxNodeInClade[0] = node.getNr();
            buf.append(node.getNr() + 1);
        }

        buf.append(("[&host="+demeList[node.getNr()]+",numTransmissions="+countList[node.getNr()]+"]"));
        buf.append(":").append(node.getLength());
        return buf.toString();
    }
    
    /**
     * CalculationNode methods *
     */
    @Override
    public void store() {
        storedLogP = logP;
        //storedLineageProbsList1=new ArrayList<conciseVector>(lineageProbsList1);
        storedPLikes=pLikes.clone();
        storedPDates=pDates.clone();
        storedPLikesNew=pLikesNew.clone();
        //System.out.println("Storing eventlist");
        //storedEventList = new ArrayList<SCEvent>();
        //for (int i = 0; i < eventList.size(); i++) {
        //	storedEventList.add(eventList.get(i));
		//}
        //System.out.println("Storing eventlist done");
        super.store();
    }

    @Override
    public void restore() {
        logP = storedLogP;
        //lineageProbsList1=new ArrayList<conciseVector>(storedLineageProbsList1);
        pLikes=storedPLikes.clone();
        pDates=storedPDates.clone();
        pLikesNew=storedPLikesNew.clone();
        //eventList=storedEventList;
        //System.out.println("Restoring eventlist");
        //eventList = new ArrayList<SCEvent>();
        //for (int i = 0; i < storedEventList.size(); i++) {
        //	eventList.add(storedEventList.get(i));
		//}
        //System.out.println("Restoring eventlist done");
        super.restore();
    }
    
    /**
     * @see beast.base.core.Loggable *
     */
    public void close(PrintStream out) {
        out.print("End;");
    }
    
//    /**
//     * how many demes are there.
//     */
//    public int getNDemes() {
//        return nDemes;
//    }
    
    /**
     * get the tree.
     */
    public MultiTypeTreeConcise getTree() {
        return mtTree;
    }
    
    
    /** obtain partial likelihoods **/
    public conciseVector getLikes(int nodeNum) {
        return pLikes[nodeNum];
    }
    
    /** obtain partial likelihoods extended over deme events **/
    public ArrayList<conciseVector> getLikesNew(int nodeNum) {
        return pLikesNew[nodeNum];
    }
    
    
    /** obtain partial likelihoods extended over deme events (last of list) **/
    public conciseVector getLikesNew(int nodeNum, Boolean t) {
    	int len=pLikesNew[nodeNum].size();
        return pLikesNew[nodeNum].get(len-1);
    }
    
    
    /** set partial likelihoods **/
    public void setLikes(conciseVector likes, int nodeNum) {
        //startEditing();
        this.pLikes[nodeNum] = likes;
    }
    
    /** add partial likelihoods to node list **/
    public void setLikesNew(conciseVector likes, int nodeNum) {
        this.pLikesNew[nodeNum].add(likes);
    }
    
    
    /**
     * Sample the number of virtual events to occur along branch.
     * 
     * @param typeStart Type at start (bottom) of branch
     * @param typeEnd Type at end (top) of branch
     * @param muL Expected unconditioned number of virtual events
     * @param Pba Probability of final type given start type
     * @param migrationModel Migration model to use.
     * @param sym flag to say whether the migration matrix is symmetric
     * @return number of virtual events.
     */
    private int drawEventCount(int typeStart, int typeEnd, double muL, double Pba, MigrationModelUniform migrationModelUniform) {
        double ex=Math.exp(-muL);
        if (typeStart== typeEnd){
        	double tot=ex+(1.0-ex-muL*ex)/migrationModelUniform.getNumDemes();
        	if (Randomizer.nextDouble()*tot>ex) return (int) nextPoissonConditional(muL, 2);
        	else return 0;
        }else{
        	double tot=(muL*ex)/(migrationModelUniform.getNumDemes()-1.0)+(1.0-ex-muL*ex)/migrationModelUniform.getNumDemes();
        	if (Randomizer.nextDouble()*tot>(muL*ex)/(migrationModelUniform.getNumDemes()-1.0)) return (int) nextPoissonConditional(muL, 2);
        	else return 1;
        }
    }
    
    /**
     * Direct method: only efficient for small lambda.
     * 
     * @param lambda
     * @return 
     */
    private double poissonian_InverseTransformSampling_conditional(double lambda, int min) {
    	int x=min;
    	double p=Math.exp(-lambda);
    	double tot=1.0;
    	for (int i=1;i<=min;i++) {
    		tot-=p;
    		p=p*lambda/i;
    	}
    	double s=p;
    	double u=Randomizer.nextDouble()*tot;
    	while (u > s){
    		x++;
            p=p * lambda / x;
            s = s + p;
    	};
        return x;
    }

    /**
     * Sample from a Poissonian distribution.  Note that samples are expressed
     * as doubles, allowing for sensible convergence to the appropriate
     * Gaussian when extremely large lambdas are used.  Be aware however that
     * systematic errors due to rounding start to creep in for lambda>1e14.
     * Can we improve on this?
     * 
     * @param lambda
     * @return Draw from Pois(lambda).
     */
    public double nextPoissonConditional(double lambda, int min) {
        if (lambda<12)
            return poissonian_InverseTransformSampling_conditional(lambda, min);
        int n=-1;
        while (n<min) n=(int)Randomizer.nextPoisson(lambda);
        return n;
    }
    
    
    
    /**
     * set one deme at the root picked at random from the probability distribution of demes at the root.
     */
    public void setRootDeme() {
    	MultiTypeNodeConcise root=(MultiTypeNodeConcise) mtTree.getRoot();
    	conciseVector probs = pLikes[root.getNr()];
    	int type=probs.sample();
        root.setNodeType(type);
        demeList[root.getNr()]=StringVersion(type);
        countList[root.getNr()]=0;
        //System.out.println("Root deme type: "+type);
    }
    
    
    /**
     * set all demes at internal nodes, picking at random from their probability conditioned on their subtree leaves, 
     * and conditioned on the deme sampled for their parent.
     */
    public void setAllDemes() {
    	//System.out.println("Starting setAllDemes");
    	updateEventSequenceToSetDemes();
    	setRootDeme();
    	MultiTypeNodeConcise root=(MultiTypeNodeConcise) mtTree.getRoot();
    	//Then make iteration
    	if ((root.getLeft()!=null)){
    		Node LC= root.getLeft();
        	double LCTime= root.getHeight() - LC.getHeight();
        	setDemeIterativeNew((MultiTypeNodeConcise) root.getLeft(),LCTime, root.getNodeType());
    	}
    	
    	if ((root.getRight()!=null)){
    		Node RC= root.getRight();
        	double RCTime= root.getHeight() - RC.getHeight();
        	setDemeIterativeNew((MultiTypeNodeConcise) root.getRight(),RCTime, root.getNodeType());
    	}
    	//System.out.println("Ending setAllDemes");
    }
    
    
    
    /**
     * Set the deme for a node, and then call the function iteratively for the leaves.
     * and conditioned on the deme sampled for their parent.
     */
    public void setDemeIterativeNew(MultiTypeNodeConcise node, double time, int parentDeme) {
    	//System.out.println("Starting setDemeIterativeNew "+node.getNr()+" date of node: "+node.getDate()+" type of parent "+parentDeme+" ");
    	int type=-1;
    	//if ((!node.isLeaf()) || sampleMig) {
    		//System.out.println("Not leaf ");
    		//calculate probabilities for node location
    		ArrayList<conciseVector> likes=pLikesNew[node.getNr()];
    		ArrayList<Double> times=pDates[node.getNr()];
    		double firstDate=node.getHeight()+time;
    		int currentDeme=parentDeme, n=0;
    		double t=0.0, lapse=0.0, sum=0.0, muL=0.0, Pba=0.0;
    		countList[node.getNr()]=0;
    		//System.out.println("There are number of time points "+likes.size());
    		for (int ca=0; ca<likes.size()-1;ca++){
    			//System.out.println("Time point "+ca);
    			//System.out.println("likes size "+likes.size());
    			//System.out.println("pDates size "+times.size());
    			conciseVector l=likes.get(likes.size()-(ca+2));
    			t = times.get(likes.size()-(ca+2));
    			lapse= firstDate-t;
    			//System.out.println("lapse "+lapse);
    			if (lapse>0.0){
    				firstDate=firstDate-lapse;
        			conciseVector p2= l.conditional(migrationModelUniform.getRate()*lapse*(migrationModelUniform.getNumDemes()-1), currentDeme);
        			sum= p2.sumV();
        			p2.scaleV(1.0/sum);
        			type=p2.sample();
        			if (sampleMig){
        		    	muL=migrationModelUniform.getRate()*lapse*(migrationModelUniform.getNumDemes()-1);
        		    	if (type==parentDeme) Pba= migrationModelUniform.getStay(lapse);
        		    	else Pba = migrationModelUniform.getLeave(lapse);
        		    	//calculate number of migrations on branch
        		    	n= drawEventCount(type, currentDeme, muL, Pba, migrationModelUniform);
        		    	countList[node.getNr()]+=n;
        	    	}
        			currentDeme=type;
        			//System.out.println("Chosen "+type+" after time "+lapse);
    			}
    		}
    		//conciseVector p2= likes.conditional(migrationModelUniform.getRate()*time*migrationModelUniform.getNumDemes(), parentDeme);
        	//double sum= p2.sumV();
        	//p2.scaleV(1.0/sum);
        	//type=p2.sample();
    		if (!node.isLeaf()) {
    			((MultiTypeNodeConcise) node).setNodeType(currentDeme);
    			demeList[node.getNr()]=StringVersion(currentDeme);
    			//System.out.println("Last Choice "+currentDeme);
    		}else{
    			type = ((MultiTypeNodeConcise) node).getNodeType();
        		demeList[node.getNr()]=StringVersion(type);
        		//System.out.println("Last Choice "+type);
    		}
    		//System.out.println("End not leaf ");
//    	}else{
//    		//System.out.println("Leaf ");
//    		type = ((MultiTypeNodeConcise) node).getNodeType();
//    		demeList[node.getNr()]=StringVersion(type);
//    		//System.out.println("End leaf ");
//    	}
    	
//    	if (sampleMig){
//	    	double muL=migrationModelUniform.getRate()*time*(migrationModelUniform.getNumDemes()-1);
//	    	double Pba;
//	    	if (type==parentDeme) Pba= migrationModelUniform.getStay(time);
//	    	else Pba = migrationModelUniform.getLeave(time);
//	    	//calculate number of migrations on branch
//	    	int n= drawEventCount(type, parentDeme, muL, Pba, migrationModelUniform);
//	    	countList[node.getNr()]=n;		
//    	}	
    	
    	//Then make iteration
    	if (node.getLeft()!= null){
    		MultiTypeNodeConcise LC= (MultiTypeNodeConcise) node.getLeft();
    		double LCTime= node.getHeight() - LC.getHeight();
    		setDemeIterativeNew(LC ,LCTime, ((MultiTypeNodeConcise) node).getNodeType());
    	}
    	
    	if (node.getRight()!= null){
    		MultiTypeNodeConcise RC= (MultiTypeNodeConcise) node.getRight();
    		double RCTime= node.getHeight() - RC.getHeight();
    		setDemeIterativeNew(RC ,RCTime, ((MultiTypeNodeConcise) node).getNodeType());
    	}
    	//System.out.println("Ending setDemeIterativeNew "+node.getNr());
    }
    
    
    
    
//    /**
//     * Set the deme for a node, and then call the function iteratively for the leaves.
//     * and conditioned on the deme sampled for their parent.
//     */
//    public void setDemeIterative(MultiTypeNodeConcise node, double time, int parentDeme) {
//    	int type=-1;
//    	if (!node.isLeaf()) {
//    		//calculate probabilities for node location
//    		conciseVector likes=pLikes[node.getNr()];
//    		conciseVector p2= likes.conditional(migrationModelUniform.getRate()*time*(migrationModelUniform.getNumDemes()-1), parentDeme);
//        	double sum= p2.sumV();
//        	p2.scaleV(1.0/sum);
//        	type=p2.sample();
//    		((MultiTypeNodeConcise) node).setNodeType(type);
//    		demeList[node.getNr()]=StringVersion(type);
//    	}else{
//    		type = ((MultiTypeNodeConcise) node).getNodeType();
//    		demeList[node.getNr()]=StringVersion(type);
//    	}
//    	
//    	if (sampleMig){
//	    	double muL=migrationModelUniform.getRate()*time*(migrationModelUniform.getNumDemes()-1);
//	    	double Pba;
//	    	if (type==parentDeme) Pba= migrationModelUniform.getStay(time);
//	    	else Pba = migrationModelUniform.getLeave(time);
//	    	//calculate number of migrations on branch
//	    	int n= drawEventCount(type, parentDeme, muL, Pba, migrationModelUniform);
//	    	countList[node.getNr()]=n;		
//    	}	
//    	
//    	//Then make iteration
//    	if (node.getLeft()!= null){
//    		MultiTypeNodeConcise LC= (MultiTypeNodeConcise) node.getLeft();
//    			double LCTime= node.getHeight() - LC.getHeight();
//    			setDemeIterative(LC ,LCTime, ((MultiTypeNodeConcise) node).getNodeType());
//    	}
//    	
//    	if (node.getRight()!= null){
//    		MultiTypeNodeConcise RC= (MultiTypeNodeConcise) node.getRight();
//    			double RCTime= node.getHeight() - RC.getHeight();
//    			setDemeIterative(RC ,RCTime, ((MultiTypeNodeConcise) node).getNodeType());
//    	}
//    	
//    }
    
    public String StringVersion(int type){
    	//if (type >= migrationModelUniform.getMinNumDemes() ) return "Unsampled";
    	if (type >= mtTree.getTypeList().size() ) return "Unsampled";
    	else return mtTree.getTypeList().get(type);
    }

    // Empty constructor as required:
    public StructuredCoalescentTreeDensityNew() { };

    @Override
    public void initAndValidate() {
    	//System.out.println("Starting initAndValidate");
        migrationModelUniform = migrationModelInput.get();
        mtTree = mtTreeInput.get();
        checkValidity = checkValidityInput.get();
        
        limitedLifespan = limitedLifespanInput.get();
        penalizeMigration = penalizeMigrationInput.get();
        int numLives2=0;
        int trueDemes=migrationModelUniform.startValues.keySet().size();
        if(limitedLifespan){
	        demeEvents= new demeEvent[trueDemes*2];
	        for (int i=0; i<trueDemes;i++){
	        	demeEvents[2*i]=new demeEvent(i,Double.POSITIVE_INFINITY,"start");
	        	demeEvents[2*i+1]=new demeEvent(i,Double.NEGATIVE_INFINITY,"end");
	        }
        }else{
        	demeEvents= new demeEvent[migrationModelUniform.getMinNumDemes()*2];
        	for (int i=0; i<migrationModelUniform.getMinNumDemes();i++){
            	demeEvents[2*i]=new demeEvent(i,Double.POSITIVE_INFINITY,"start");
            	demeEvents[2*i+1]=new demeEvent(i,Double.NEGATIVE_INFINITY,"end");
            }
        }
        
        if (migrationModelUniform.startValues!=null){
	        for(String s : migrationModelUniform.startValues.keySet()){
	        	int i=mtTree.getTypeList().indexOf(s);
	        	demeEvent de = new demeEvent(i,migrationModelUniform.startValues.get(s),"start");
	        	demeEvents[2*i]=de;
	        	numLives2+=1;
	        	meanLife+=migrationModelUniform.startValues.get(s);
	        	if (migrationModelUniform.startValues.get(s)>earliestBirth) {
	        		earliestBirth=migrationModelUniform.startValues.get(s);
	        	}
	        }
        }else{
        	if (limitedLifespan){
        		System.out.printf("Error: you need epi data (host deme death dates) to define mean host lifespan if limitedLifespan=True\n");
        		System.exit(0);
        	}
        }

        if (migrationModelUniform.endValues!=null){
	        for(String s : migrationModelUniform.endValues.keySet()){
	        	int i=mtTree.getTypeList().indexOf(s);
	        	demeEvent de = new demeEvent(i,migrationModelUniform.endValues.get(s),"end");
	        	demeEvents[2*i+1]=de;
	        	meanLife-=migrationModelUniform.endValues.get(s);
	        	if (migrationModelUniform.endValues.get(s)<latestDeath) {
	        		latestDeath=migrationModelUniform.endValues.get(s);
	        	}
	        }
	        meanLife=meanLife/numLives2;
        }else{
        	if (limitedLifespan){
        		System.out.printf("Error: you need epi data (host deme birth dates) to define mean host lifespan if limitedLifespan=True\n");
        		System.exit(0);
        	}
        }
        
        eventList = new ArrayList<SCEvent>();
        lineageCountList = new ArrayList<conciseVector>();
        lineageCountSquareList = new ArrayList<conciseVector>();
        lineageProbsList1 = new ArrayList<conciseVector>();
        lineageProbsList2 = new ArrayList<conciseVector>();
        //lineageProbsListAll = new ArrayList<conciseVector>();
        lineageCountListAfter = new ArrayList<conciseVector>();
        lineageCountSquareListAfter = new ArrayList<conciseVector>();
        
        pLikes = new conciseVector[mtTree.getNodeCount()];
        pLikesNew= new  ArrayList[mtTree.getNodeCount()];
        for (int i =0; i<mtTree.getNodeCount();i++) pLikesNew[i]= new ArrayList<conciseVector>();
        pDates= new  ArrayList[mtTree.getNodeCount()];
        for (int i =0; i<mtTree.getNodeCount();i++) pDates[i]= new ArrayList<Double>();
        //numberList = new ArrayList<Integer>();
        countList = new Integer[mtTree.getNodeCount()];
        demeList = new String[mtTree.getNodeCount()];
        //System.out.println("Ending initAndValidate");
    }
    

    
    
    
    @Override
    public double calculateLogP() {
    	//System.out.println("Starting calculateLogP");
    	//System.out.println("\n Calculating logP\n");
        // Check validity of tree if required:
        if (checkValidity && !mtTree.isValid())
            return Double.NEGATIVE_INFINITY;
        
        migrationModelUniform.updateMatrices();
        
        //int nTypes = migrationModelUniform.getNumDemes();
        //nDemes=nTypes;
        
        // Ensure sequence of events is up-to-date:
        updateEventSequence();

        // Start from the tips of the tree, working up.
        logP = 0;
        
        //Find first sample event
        int preEventIdx=0;
        while (eventList.get(preEventIdx).kind!=SCEventKind.SAMPLE) preEventIdx++;
        
        //Find last coalescent event
        int lastEvent=eventList.size()-1;
        while (eventList.get(lastEvent).kind!=SCEventKind.COALESCE) lastEvent--;
        
        // Note that the first event is always a sample. We begin at the first
        // _interval_ and the event following that interval.
        for (int eventIdx = 1; (eventIdx+preEventIdx)<=lastEvent; eventIdx++) {
        	//System.out.println("\n New event for log likelihood. Number "+eventIdx+" time "+eventList.get(eventIdx+preEventIdx).time+" type "+eventList.get(eventIdx+preEventIdx).kind+" LogP until now is : "+logP);
            SCEvent event = eventList.get(eventIdx+preEventIdx);
            
            //Counts of expected lineages and squared ones for present and next event times.
            conciseVector lineageCount = lineageCountListAfter.get(eventIdx-1);
            conciseVector lineageCountSquare = lineageCountSquareListAfter.get(eventIdx-1);
            conciseVector lineageCount2 = lineageCountList.get(eventIdx);
            conciseVector lineageCountSquare2 = lineageCountSquareList.get(eventIdx);
            
            double delta_t = event.time - eventList.get(eventIdx+preEventIdx-1).time;
            
            //System.out.println("Before interval likelihood "+logP+" time "+eventList.get(eventIdx+preEventIdx-1).time);
            
            //conciseVector lineageProbs1 = lineageProbsList1.get(eventIdx);
            //System.out.println("LineageProbLis1 is "+lineageProbsList1.get(eventIdx)+" ");
            //if (lineageProbsList1.get(eventIdx)!=null){
            //	for (int i=0;i<lineageProbsList1.get(eventIdx).diffValues.size();i++){
            //		System.out.println("Value "+i+" is "+lineageProbsList1.get(eventIdx).get(i)+" ");
            //	}
            //}
            
        	//calculate sub-intervals of time to be considered
        	double delta_t1=0.0, delta_t2=0.0;
        	delta_t1=delta_t/2.0;
    		delta_t2=delta_t/2.0;

        	//add first interval half contribution to likelihood
        	double totRate=0.0;
        	if (delta_t1>0) {
        		//calculate total coalescent rate at lower sub-interval
        		conciseVector lC= lineageCount.square();
        		totRate+=lC.sumV();
        		totRate-=lineageCountSquare.sumV();
        		totRate=totRate/(2.0*migrationModelUniform.getPopSize());
        		logP += -delta_t1*totRate;
        	}
        	//System.out.println("Pre-event first interval likelihood "+logP);
			
        	//add second interval half contribution to likelihood
        	if (delta_t2>0) {
        		//calculate total coalescent rate at higher sub-interval
            	totRate=0.0;
            	conciseVector lC= lineageCount2.square();
        		totRate+=lC.sumV();
        		totRate-=lineageCountSquare2.sumV();
        		totRate=totRate/(2.0*migrationModelUniform.getPopSize());
        		logP += -delta_t2*totRate;
        		if (totRate<-0.00001){
        			System.out.println(" Problem: totRate negative ");
        			System.exit(0);
        		}
        	}
        	//System.out.println("Pre-event second interval likelihood "+logP);
        	
            // Event contribution:
            switch (event.kind) {
                case COALESCE:
                	conciseVector lineageProbs1 = lineageProbsList1.get(eventIdx);
                	conciseVector lineageProbs2 = lineageProbsList2.get(eventIdx);
                	conciseVector p1= lineageProbs1.entrywiseProd(lineageProbs2);
                	double coalRate=p1.sumV()/migrationModelUniform.getPopSize();
                    logP += Math.log(coalRate);
                    //System.out.println("Coalesce likelihood "+logP+" at time "+event.time);
                    break;

                case SAMPLE:
                	//System.out.println("Sample ");
                    break;
                case ADD_DEME:
                	if (penalizeMigration){
	                	//System.out.println("Trying to improve likelihood ");
	                	conciseVector lineageProbs3 = lineageProbsList1.get(eventIdx);
	                	List<Double> probs=new ArrayList<Double>();
	                	int maxInd=-1;
	                	double max=0.0;
	                	for (int linIdx = 0; linIdx<lineageProbs3.diffValues.size(); linIdx++) {
	                		//System.out.println("For "+linIdx+" value "+lineageProbs3.diffValues.get(linIdx));
	                		//System.out.println(lineageProbs3.diffValues.get(linIdx)+" \n");
	                		if (lineageProbs3.diffValues.get(linIdx)>1.001 || lineageProbs3.diffValues.get(linIdx)<-0.001){
	                			System.out.println("Problem at lineage probability "+lineageProbs3.diffValues.get(linIdx)+" time "+event.time);
	                			System.exit(0);
	                		}
	                		//System.out.println("Probability of lineage "+linIdx+" to be in deme to be removed is "+lineageProbs3.diffValues.get(linIdx));
	                		if (lineageProbs3.diffValues.get(linIdx)>max){
	                			max=lineageProbs3.diffValues.get(linIdx);
	                			maxInd=linIdx;
	                		}
	                		probs.add(lineageProbs3.diffValues.get(linIdx));
	                		//logP += Math.log(1.0-lineageProbs3.diffValues.get(linIdx));
	                	}
	                	for (int linIdx = 0; linIdx<probs.size(); linIdx++) {
	                		if (linIdx!=maxInd) logP += Math.log(1.0-probs.get(linIdx));
	                	}
	                	//System.out.println("ADD DEME likelihood "+logP+" at time "+event.time);
	                	//System.out.println("Likelihood improved");
                	}
    				break;
    			case REMOVE_DEME:
    				//System.out.println("Remove deme ");
    				break;
            }
            //System.out.println("likelihood "+logP+" at time "+event.time);
        }
        if (logP>100000000000000000000000000000000000000000000000000.0) {
        	System.out.println("likelihood getting too high: "+logP);
        	System.exit(0);
        }
        //System.out.println("Final logP: "+logP);
        //System.exit(0);
        //System.out.println("Ending calculateLogP");
        return logP;
    }

    
    
    
    
    
    public List<Integer> addSorted(List<Integer> list, Integer e){
    	int i=0;
    	while (i<list.size()) {
    		if (list.get(i)>=e){
    			list.add(i,e);
    			return list;
    		}
    		i++;
    	}
    	list.add(e);
    	return list;
    }
    
    public List<Integer> removeElement(List<Integer> list, Integer e){
    	int i=0;
    	while (i<list.size()) {
    		if (list.get(i).equals(e)){
    		//if (list.get(i)==e){
    			list.remove((Integer)e);
    			return list;
    		}
    		i++;
    	}
    	return list;
    }
    
    /**
     * Determines the sequence of coalescence and sampling events
     * which make up the Volz tree.
     */
    public void updateEventSequence() {
    	//System.out.println("Starting updateEventSequence");
        // Clean up previous list:
        eventList.clear();
        lineageCountList.clear();
        lineageCountSquareList.clear();
        lineageCountListAfter.clear();
        lineageCountSquareListAfter.clear();
        lineageProbsList1.clear();
        lineageProbsList2.clear();
        Node rootNode = mtTree.getRoot();
        int nTypes= migrationModelUniform.getNumDemes();

        limitedLifespan = limitedLifespanInput.get();
        penalizeMigration = penalizeMigrationInput.get();
        //totDemes=migrationModelUniform.getNumDemes();
        int trueDemes=migrationModelUniform.startValues.keySet().size();
        int extraDemes=migrationModelUniform.getNumDemes()-trueDemes ;
        int lenExtra=0;
        int numLives= (int) (((earliestBirth-latestDeath)/meanLife)+2);
        if (limitedLifespan) {lenExtra=(extraDemes)*(numLives*2+1);}else{lenExtra=extraDemes*2;}
        demeEvent[] demeEventsExtra= new demeEvent[lenExtra];
        //Setting events for extra unsampled demes in case limitedLifespan=true
        if (limitedLifespan) {
        	double eps= meanLife*0.000001;
        	double piece=meanLife;
        	if (extraDemes>0) piece=piece/(extraDemes);
        	for (int i=0; i<extraDemes;i++){
        		for (int j=0; j<numLives;j++){
        			demeEvent de = new demeEvent(trueDemes+i, latestDeath + meanLife*(j) + piece*i -eps,"start");
        			demeEventsExtra[2*(i*numLives +j)]=de;
        			demeEvent de2 = new demeEvent(trueDemes+i, latestDeath + meanLife*(j-1) + piece*i,"end");
        			demeEventsExtra[2*(i*numLives +j) +1]=de2;
        		}
        	}
        	for (int i=0; i<(extraDemes);i++){//setting extra demes forever open at the top: this saves time if you have multiple introductions
        		demeEvent de = new demeEvent(trueDemes+i, latestDeath + meanLife*(numLives-1) + piece*i,"end");
        		demeEventsExtra[(extraDemes)*(numLives*2)+i]=de;
        	}	
        }else{
        	for (int i=0; i<extraDemes;i++){
            	demeEventsExtra[2*i]=new demeEvent(i,Double.POSITIVE_INFINITY,"start");
            	demeEventsExtra[2*i+1]=new demeEvent(i,Double.NEGATIVE_INFINITY,"end");
            }
        }
        //Join the two events lists
    	demeEvent[] demeEvents2= new demeEvent[demeEvents.length + lenExtra];
    	for (int i=0; i<demeEvents.length;i++){
    		demeEvents2[i]=demeEvents[i];
    	}
    	for (int i=0; i<lenExtra;i++){
    		demeEvents2[i+demeEvents.length]=demeEventsExtra[i];
    	}
        
        demeEventsList=Arrays.asList(demeEvents2);
        Collections.sort(demeEventsList, new Comparator<demeEvent>() {
            public int compare(demeEvent o1, demeEvent o2) {
                return o2.time.compareTo(o1.time);
            }
        });
        Collections.reverse(demeEventsList);
        
        

        // Initialise map of active nodes to active change indices:
        Map<Node, Integer> changeIdx = new HashMap<Node, Integer>();
        changeIdx.put(rootNode, -1);
        
        int demeIndex=0;
        // Calculate event sequence:
        while ((!changeIdx.isEmpty()) || (demeIndex<demeEventsList.size())) {
        	demeEvent nextDemeEvent;
        	SCEvent nextEvent = new SCEvent();
        	if (!demeEventsList.isEmpty()){
        		nextDemeEvent= demeEventsList.get(demeEventsList.size()-(1+demeIndex));
        		nextEvent.time = nextDemeEvent.time;
        		nextEvent.type = nextDemeEvent.index;
        		if (nextDemeEvent.type=="start") nextEvent.kind = SCEventKind.ADD_DEME;
        		else if (nextDemeEvent.type=="end") nextEvent.kind = SCEventKind.REMOVE_DEME;
        	}else            nextEvent.time = Double.NEGATIVE_INFINITY;
            nextEvent.node = rootNode; // Initial assignment not significant

            // Determine next event
            for (Node node : changeIdx.keySet())
                if (changeIdx.get(node)<0) {
                    if (node.isLeaf()) {
                        // Next event is a sample
                        if (node.getHeight()>nextEvent.time) {
                            nextEvent.time = node.getHeight();
                            nextEvent.kind = SCEventKind.SAMPLE;
                            nextEvent.type = ((MultiTypeNodeConcise)node).getNodeType();
                            nextEvent.node = node;
                        }
                    } else {
                        // Next event is a coalescence
                        if (node.getHeight()>nextEvent.time) {
                            nextEvent.time = node.getHeight();
                            nextEvent.kind = SCEventKind.COALESCE;
                            nextEvent.node = node;
                            nextEvent.type = ((MultiTypeNodeConcise)node).getNodeType();
                        }
                    }
                }
            

            // Update active node list (changeIdx) and lineage count appropriately:
            switch (nextEvent.kind) {
                case COALESCE:
                	Node leftChild = nextEvent.node.getLeft();
                    Node rightChild = nextEvent.node.getRight();
                    changeIdx.remove(nextEvent.node);
                    changeIdx.put(leftChild, -1);
                    changeIdx.put(rightChild, -1);
                    break;
                case SAMPLE:
                    changeIdx.remove(nextEvent.node);
                    break;
                case ADD_DEME:
                	demeIndex+=1;
         			break;
     			case REMOVE_DEME:
     				demeIndex+=1;
     				break;
            }
            // Add event to list:
            eventList.add(nextEvent);
        }

        // Reverse event and lineage count lists (order them from tips to root):
        eventList = Lists.reverse(eventList);
        
        //Initialize track of inactive demes
        int eventInd=0;
        List<Integer> inactiveDemes= new ArrayList<Integer>();
        int minD= migrationModelUniform.getMinNumDemes();
        for (int i =0;i<minD;i++) inactiveDemes= addSorted(inactiveDemes,i);
        while (eventList.get(eventInd).kind!=SCEventKind.SAMPLE){
        	if (eventList.get(eventInd).kind==SCEventKind.REMOVE_DEME) inactiveDemes= removeElement(inactiveDemes, eventList.get(eventInd).type);
        	if (eventList.get(eventInd).kind==SCEventKind.ADD_DEME) inactiveDemes=addSorted(inactiveDemes, eventList.get(eventInd).type);
        	eventInd+=1;
        }
        
        // Initialise lineage count per colour array:
        SCEvent nextEvent = eventList.get(eventInd);
        double coalTime=nextEvent.time;
        conciseVector lineageCount = new conciseVector(nTypes,nextEvent.type,inactiveDemes);
        
        if ((inactiveDemes.contains(nextEvent.type))) {
        	System.out.println("Deme (" + nextEvent.type + ") is not available to be sampled. Wrong epi/sampling dates?");
        	System.exit(0);
        }
        
        //Initialize first node
        MultiTypeNodeConcise firstNode = (MultiTypeNodeConcise) nextEvent.node;
        firstNode.setCacheIndex(0);
        setLikes(lineageCount.copy(),firstNode.getNr());
        //setLikesNew(lineageCount.copy(),firstNode.getNr());
        
        //Initialize square count
        conciseVector lineageCountSquare = lineageCount.copy();
        lineageCountList.add(null);
        lineageCountSquareList.add(null);
        lineageCountListAfter.add(lineageCount.copy());
        lineageCountSquareListAfter.add(lineageCount.copy());
        //initialize cache of probabilities for lineages to be in demes
        List<conciseVector> lineageProbs = new ArrayList<conciseVector>();
        lineageProbs.add(lineageCount.copy());
        lineageProbsList1.add(null);
        lineageProbsList2.add(null);
        //List to keep track of how many lineages are being cached on the same probability vector.
        List<Integer> numCached = new ArrayList<Integer>();
        numCached.add(1);
        //List to keep track of which types the cache represent, if they are samples just inserted
        List<Integer> typeCached = new ArrayList<Integer>();
        typeCached.add(nextEvent.type);
        //Count expected number of lineages in each deme, etc, going from bottom to top
        //double[] pMatrix = new double[nTypes*nTypes];
        conciseVector newLineageCount = new conciseVector(nTypes,inactiveDemes);
        
        //Find last coalescent event
        int lastEvent=eventList.size()-1;
        while (eventList.get(lastEvent).kind!=SCEventKind.COALESCE) lastEvent--;

        //System.out.println("Start for cycle on events ");
        for (int c = eventInd+ 1; c<=lastEvent; c++) {
        	nextEvent = eventList.get(c);
        	//System.out.println("Event "+nextEvent.time+" "+nextEvent.type+" "+nextEvent.kind+" ");
        	if ((nextEvent.time != coalTime) || (nextEvent.kind==SCEventKind.COALESCE)){
        		double delta_t = nextEvent.time - coalTime;
        		newLineageCount=lineageCount.matrixMul(delta_t*migrationModelUniform.getRate()*(migrationModelUniform.getNumDemes()-1));//*(nTypes-inactiveDemes.size())
        		lineageCount=newLineageCount;
        		lineageCountList.add(lineageCount.copy());
        		
        		//find new expected squared counts of lineages in demes
        		lineageCountSquare= new conciseVector(nTypes, inactiveDemes);
        		for (int ca = 0; ca<numCached.size(); ca++) {
        			if (numCached.get(ca)>0){
        				conciseVector prob = lineageProbs.get(ca);
        				conciseVector newProb = prob.matrixMul(delta_t*migrationModelUniform.getRate()*(migrationModelUniform.getNumDemes()-1));//*(nTypes-inactiveDemes.size())
            			lineageProbs.set(ca, newProb);
            			if (newProb.sumV()>1.0001){
        						System.out.println("Found strange probability in updating no event segment "+newProb.sumV());
                    			System.exit(0);
        				}
            			conciseVector sV= newProb.square();
            			sV.scaleV(numCached.get(ca));
            			lineageCountSquare.sum(sV);
            		}
        		}
        		lineageCountSquareList.add(lineageCountSquare.copy());
        		
        		
				//update counts after the event (not used for present coalescing rates)
        		switch (nextEvent.kind) {
                case COALESCE:
                    MultiTypeNodeConcise leftChild = (MultiTypeNodeConcise) nextEvent.node.getLeft();
                    MultiTypeNodeConcise rightChild = (MultiTypeNodeConcise) nextEvent.node.getRight();
                    int index1=leftChild.getCacheIndex();
                    int index2=rightChild.getCacheIndex();
                    
                    //pass probability vectors right before coalescence for likelihood calculation
                    lineageProbsList1.add(lineageProbs.get(index1).copy());
                    lineageProbsList2.add(lineageProbs.get(index2).copy());
                    if (lineageProbs.get(index1).sumV()>1.0001){
                    	System.out.println("Adding wrong probabilities 1 "+lineageProbs.get(index1).sumV());
                    }
                    
                    //calculate the probability vector for the new coalesced lineage, following the coalescent event
                    conciseVector v = lineageProbs.get(index1).copy();
                    v=v.entrywiseProd(lineageProbs.get(index2));
                    v.scaleV(1.0/migrationModelUniform.getPopSize());
                    double tot= v.sumV();
                    v.scaleV(1.0/tot);
                    
                    lineageProbs.add(v.copy());
                    if (v.sumV()>1.0001){
    						System.out.println("Found strange probability in COALESCE "+v.sumV());
    						v = lineageProbs.get(index1).copy();
    						System.out.println(" "+v.sumV()+"\n\n");
    						conciseVector v2 =lineageProbs.get(index2);
    						System.out.println(" "+v2.sumV()+"\n\n");
    						v=v.entrywiseProd(v2);
    						v.scaleV(1.0/migrationModelUniform.getPopSize());
    						tot= v.sumV();
    	                    v.scaleV(1.0/tot);
    	                    System.out.println(" "+v.sumV()+"\n\n");
                			System.exit(0);
    				}
                    setLikes(v.copy(), nextEvent.node.getNr());
                    //setLikesNew(v.copy(), nextEvent.node.getNr());
                    MultiTypeNodeConcise coalesced = (MultiTypeNodeConcise) nextEvent.node;
                    coalesced.setCacheIndex(lineageProbs.size()-1);
                    numCached.add(1);
                    numCached.set(index1, numCached.get(index1)-1);
                    numCached.set(index2, numCached.get(index2)-1);
                    for (int t = 0; t<typeCached.size(); t++) typeCached.set(t,-1);
                    typeCached.add(-1);
                	
                    
                    //Final update of counts, following the coalescent event
                	lineageCount.sum(v);
                	conciseVector v2=lineageProbs.get(index1).copy();
                	v2.sum(lineageProbs.get(index2));
                	v2.scaleV(-1.0);
                	lineageCount.sum(v2);
                	
                	lineageCountSquare.sum(v.square());
                	v2=lineageProbs.get(index1).square();
                	v2.sum(lineageProbs.get(index2).square());
                	v2.scaleV(-1.0);
                	lineageCountSquare.sum(v2);
                	
                	///put probability distribution of root!!!
                    if (c==(eventList.size()-1)){
                    	lineageProbsList1.add(v.copy());
                    	lineageProbsList2.add(v.copy());
                    }
                    //System.out.println("Coalescence new time point, lineageProbList1 is ");
                    //for (int i=0;i<lineageProbs.get(index1).diffIndeces.size();i++){
                    //	System.out.println(" "+lineageProbs.get(index1).diffIndeces.get(i)+" ");
                    //}
                    break;

                case SAMPLE:
                	//Adding a sample at a new time point
                	numCached.add(1);
                	for (int t = 0; t<typeCached.size(); t++) typeCached.set(t,-1);
                    typeCached.add(nextEvent.type);
                    //System.out.println("New sample at new time point, of type "+nextEvent.type);
                    lineageProbsList1.add(null);
                    lineageProbsList2.add(null);
                    lineageCount.add(nextEvent.type,1.0);
                    lineageCountSquare.add(nextEvent.type,1.0);
                    conciseVector probs = new conciseVector(nTypes, nextEvent.type, inactiveDemes);
        	        lineageProbs.add(probs.copy());
        	        setLikes(probs.copy(), nextEvent.node.getNr());
        	        MultiTypeNodeConcise sampled = (MultiTypeNodeConcise) nextEvent.node;
                    sampled.setCacheIndex(lineageProbs.size()-1);
                    break;
                    
                case REMOVE_DEME:
                	//Adding a deme at a new time point (as it is backward in time, extinction means adding a deme)
                	for (int t = 0; t<typeCached.size(); t++) typeCached.set(t,-1);
                	//System.out.println("End of infectiousness for deme "+nextEvent.type);
                	inactiveDemes=removeElement(inactiveDemes, (Integer)nextEvent.type);
                    lineageProbsList1.add(null);
                    lineageProbsList2.add(null);
                    lineageCount.addDeme(nextEvent.type);
                    lineageCountSquare.addDeme(nextEvent.type);
                    for (int ca = 0; ca<numCached.size(); ca++) {
            			if (numCached.get(ca)>0){
            				lineageProbs.get(ca).addDeme(nextEvent.type);
            				if(lineageProbs.get(ca).sumV()>1.0001){
            						System.out.println("Found strange probability in updating REMOVE DEME "+lineageProbs.get(ca).sumV());
                        			System.exit(0);
            				}
            			}
            		}
                    break;
                    
                case ADD_DEME:
                	//Removing a deme at a new time point (as it is backward in time, extinction means adding a deme)
                	//System.out.println("Trying to record probabilities for likelihood improvement ");
                	for (int t = 0; t<typeCached.size(); t++) typeCached.set(t,-1);
                	inactiveDemes=addSorted(inactiveDemes, nextEvent.type);
                	ArrayList<Double> lP=new ArrayList<Double>();
                	for (int ca = 0; ca<numCached.size(); ca++) {
            			if (numCached.get(ca)>0){
            				for (int t = 0; t<numCached.get(ca); t++) lP.add(lineageProbs.get(ca).get(nextEvent.type));
            				if(lineageProbs.get(ca).get(nextEvent.type)>1.0001){
            						System.out.println("Found strange probability in updating ADD DEME "+lineageProbs.get(ca).sumV());
                        			System.exit(0);
            				}
            			}
            		}
                	conciseVector cV= new conciseVector(lP);
                    lineageProbsList1.add(cV);
                    lineageProbsList2.add(null);
                    lineageCount.removeDeme(nextEvent.type);
                    lineageCountSquare= new conciseVector(nTypes, inactiveDemes);
            		for (int ca = 0; ca<numCached.size(); ca++) {
            			if (numCached.get(ca)>0){
            				lineageProbs.get(ca).removeDeme(nextEvent.type);
            				if(lineageProbs.get(ca).sumV()>1.0001){
            						System.out.println("Found strange probability in updating ADD DEME "+lineageProbs.get(ca).sumV());
                        			System.exit(0);
            				}
                			conciseVector sV= lineageProbs.get(ca).square();
                			sV.scaleV(numCached.get(ca));
                			lineageCountSquare.sum(sV);
            			}
            		}
                	//System.out.println("Start of infectiousness for deme "+nextEvent.type+" lineageProbList1 addition is ");
                	//for (int i=0;i<cV.diffIndeces.size();i++){
                    //	System.out.println(" "+cV.diffIndeces.get(i)+" ");
                    //}
                	//System.out.println("lP instead is ");
                	//for (int i=0;i<lP.size();i++){
                    //	System.out.println(" "+lP.get(i)+" ");
                    //}
            		//System.out.println("probabillities recorded ");
                    break;
        		}
        		
        		
        	}else{     //new sampling at the same time point
        		lineageCountList.add(lineageCount.copy());
    			lineageCountSquareList.add(lineageCountSquare.copy());
    	        switch (nextEvent.kind) {
    	        case COALESCE:
    	        	//Should not be here
    	        	System.out.println("Coalescent event same time, should not be here!!! ");
    	        	lineageProbsList1.add(null);
        	        lineageProbsList2.add(null);
    	        	break;
    	        case SAMPLE:
    	        	lineageProbsList1.add(null);
        	        lineageProbsList2.add(null);
	        		int type = nextEvent.type;
	        		//System.out.println("New sample at same time point, of type "+nextEvent.type);
	        		int found=0;
	        		for (int t = 0; t<typeCached.size(); t++) {
	        			if (type==typeCached.get(t)){//re-using the same cached probability values
	        				found=1;
	        				numCached.set(t,numCached.get(t)+1);
	        				MultiTypeNodeConcise sampled = (MultiTypeNodeConcise) nextEvent.node;
	                        sampled.setCacheIndex(t);
	        				break;
	        			}
	        		}
	        		conciseVector probs= new conciseVector(nTypes,type, inactiveDemes);
	    			setLikes(probs.copy(), nextEvent.node.getNr());
	    			//setLikesNew(probs.copy(), nextEvent.node.getNr());
	        		if (found==0){         // a new leaf of a new location is added
	        			typeCached.add(type);
	        			numCached.add(1);
	        	        lineageProbs.add(probs.copy());
	        	        MultiTypeNodeConcise sampled = (MultiTypeNodeConcise) nextEvent.node;
	                    sampled.setCacheIndex(lineageProbs.size()-1);
	        		}
	        		lineageCount.add(type,1.0);
	    			lineageCountSquare.add(type,1.0);
	    			
	    			break;
	    		
    	        case REMOVE_DEME:
    	        	lineageProbsList1.add(null);
        	        lineageProbsList2.add(null);
                	//Adding (backward in time) a deme at a new time point (backward in time, extinction means adding a deme and colonization means removing a deme)
                	for (int t = 0; t<typeCached.size(); t++) typeCached.set(t,-1);
                	inactiveDemes=removeElement(inactiveDemes, nextEvent.type);
                    lineageCount.addDeme(nextEvent.type);
                    lineageCountSquare.addDeme(nextEvent.type);
                    for (int ca = 0; ca<numCached.size(); ca++) {
            			if (numCached.get(ca)>0){
            				lineageProbs.get(ca).addDeme(nextEvent.type);
            				if(lineageProbs.get(ca).sumV()>1.0001){
        						System.out.println("Found strange probability in updating REMOVE DEME same time "+lineageProbs.get(ca).sumV());
                    			System.exit(0);
            				}
            			}
            		}
                    //System.out.println("End of infectiousness (same time) for deme "+nextEvent.type);
                    break;
                    
                case ADD_DEME:
                	//System.out.println("Trying to record probabilities for likelihood improvement, same time");
                	ArrayList<Double> lP=new ArrayList<Double>();
                	for (int ca = 0; ca<numCached.size(); ca++) {
            			if (numCached.get(ca)>0){
            				for (int t = 0; t<numCached.get(ca); t++) lP.add(lineageProbs.get(ca).get(nextEvent.type));
            				if(lineageProbs.get(ca).get(nextEvent.type)>1.0001){
            						System.out.println("Found strange probability in updating ADD DEME "+lineageProbs.get(ca).sumV());
                        			System.exit(0);
            				}
            			}
            		}
                	conciseVector cV= new conciseVector(lP);
                    lineageProbsList1.add(cV);
                    lineageProbsList2.add(null);
                	//Removing (backwardward in time) a deme at a new time point (backward in time, extinction means adding a deme and colonization means removing a deme)
                	for (int t = 0; t<typeCached.size(); t++) typeCached.set(t,-1);
                	inactiveDemes=addSorted(inactiveDemes, nextEvent.type);
        			lineageCount.removeDeme(nextEvent.type);
        			lineageCountSquare= new conciseVector(nTypes, inactiveDemes);
            		for (int ca = 0; ca<numCached.size(); ca++) {
            			if (numCached.get(ca)>0){
                			lineageProbs.get(ca).removeDeme(nextEvent.type);
                			if(lineageProbs.get(ca).sumV()>1.0001){
        						System.out.println("Found strange probability in updating ADD DEME same time "+lineageProbs.get(ca).sumV());
                    			System.exit(0);
                			}
                			conciseVector sV= lineageProbs.get(ca).square();
                			sV.scaleV(numCached.get(ca));
                			lineageCountSquare.sum(sV);
            			}
            		}
//            		System.out.println("Start of infectiousness (same time) for deme "+nextEvent.type+" lineageProbList1 addition is ");
//                	for (int i=0;i<cV.diffIndeces.size();i++){
//                    	System.out.println(" "+cV.diffIndeces.get(i)+" ");
//                    }
//                	System.out.println("lP instead is ");
//                	for (int i=0;i<lP.size();i++){
//                    	System.out.println(" "+lP.get(i)+" ");
//                    }
            		//System.out.println("Recorded probabilities for likelihood improvement, same time");
                    break;
	    			
    			
    	        }
    			
        	}
        	
        	lineageCountListAfter.add(lineageCount.copy());
            lineageCountSquareListAfter.add(lineageCountSquare.copy());
        	coalTime=nextEvent.time;
        	
        }
        //System.out.println("Ending updateEventSequence");
    }
    
    
    
    
    /**
     * Nicola: Going through events from bottom to top again
     * but this time recording all lineages likelihood at all events
     * to facilitate accurate sampling of states at internal nodes.
     */
    public void updateEventSequenceToSetDemes() {
    	
    	eventList.clear();
        Node rootNode = mtTree.getRoot();
        int nTypes= migrationModelUniform.getNumDemes();

        limitedLifespan = limitedLifespanInput.get();
        penalizeMigration = penalizeMigrationInput.get();
        //totDemes=migrationModelUniform.getNumDemes();
        int trueDemes=migrationModelUniform.startValues.keySet().size();
        int extraDemes=migrationModelUniform.getNumDemes()-trueDemes ;
        int lenExtra=0;
        int numLives= (int) (((earliestBirth-latestDeath)/meanLife)+2);
        if (limitedLifespan) {lenExtra=(extraDemes)*(numLives*2+1);}else{lenExtra=extraDemes*2;}
        demeEvent[] demeEventsExtra= new demeEvent[lenExtra];
        //Setting events for extra unsampled demes in case limitedLifespan=true
        if (limitedLifespan) {
        	double eps= meanLife*0.000001;
        	double piece=meanLife;
        	if (extraDemes>0) piece=piece/(extraDemes);
        	for (int i=0; i<extraDemes;i++){
        		for (int j=0; j<numLives;j++){
        			demeEvent de = new demeEvent(trueDemes+i, latestDeath + meanLife*(j) + piece*i -eps,"start");
        			demeEventsExtra[2*(i*numLives +j)]=de;
        			demeEvent de2 = new demeEvent(trueDemes+i, latestDeath + meanLife*(j-1) + piece*i,"end");
        			demeEventsExtra[2*(i*numLives +j) +1]=de2;
        		}
        	}
        	for (int i=0; i<(extraDemes);i++){//setting extra demes forever open at the top: this saves time if you have multiple introductions
        		demeEvent de = new demeEvent(trueDemes+i, latestDeath + meanLife*(numLives-1) + piece*i,"end");
        		demeEventsExtra[(extraDemes)*(numLives*2)+i]=de;
        	}	
        }else{
        	for (int i=0; i<extraDemes;i++){
            	demeEventsExtra[2*i]=new demeEvent(i,Double.POSITIVE_INFINITY,"start");
            	demeEventsExtra[2*i+1]=new demeEvent(i,Double.NEGATIVE_INFINITY,"end");
            }
        }
        //Join the two events lists
    	demeEvent[] demeEvents2= new demeEvent[demeEvents.length + lenExtra];
    	for (int i=0; i<demeEvents.length;i++){
    		demeEvents2[i]=demeEvents[i];
    	}
    	for (int i=0; i<lenExtra;i++){
    		demeEvents2[i+demeEvents.length]=demeEventsExtra[i];
    	}
        
        demeEventsList=Arrays.asList(demeEvents2);
        Collections.sort(demeEventsList, new Comparator<demeEvent>() {
            public int compare(demeEvent o1, demeEvent o2) {
                return o2.time.compareTo(o1.time);
            }
        });
        Collections.reverse(demeEventsList);
        

        // Initialise map of active nodes to active change indices:
        Map<Node, Integer> changeIdx = new HashMap<Node, Integer>();
        changeIdx.put(rootNode, -1);
        
        int demeIndex=0;
        // Calculate event sequence:
        while ((!changeIdx.isEmpty()) || (demeIndex<demeEventsList.size())) {
        	demeEvent nextDemeEvent;
        	SCEvent nextEvent = new SCEvent();
        	if (!demeEventsList.isEmpty()){
        		nextDemeEvent= demeEventsList.get(demeEventsList.size()-(1+demeIndex));
        		nextEvent.time = nextDemeEvent.time;
        		nextEvent.type = nextDemeEvent.index;
        		if (nextDemeEvent.type=="start") nextEvent.kind = SCEventKind.ADD_DEME;
        		else if (nextDemeEvent.type=="end") nextEvent.kind = SCEventKind.REMOVE_DEME;
        	}else            nextEvent.time = Double.NEGATIVE_INFINITY;
            nextEvent.node = rootNode; // Initial assignment not significant

            // Determine next event
            for (Node node : changeIdx.keySet())
                if (changeIdx.get(node)<0) {
                    if (node.isLeaf()) {
                        // Next event is a sample
                        if (node.getHeight()>nextEvent.time) {
                            nextEvent.time = node.getHeight();
                            nextEvent.kind = SCEventKind.SAMPLE;
                            nextEvent.type = ((MultiTypeNodeConcise)node).getNodeType();
                            nextEvent.node = node;
                        }
                    } else {
                        // Next event is a coalescence
                        if (node.getHeight()>nextEvent.time) {
                            nextEvent.time = node.getHeight();
                            nextEvent.kind = SCEventKind.COALESCE;
                            nextEvent.node = node;
                            nextEvent.type = ((MultiTypeNodeConcise)node).getNodeType();
                        }
                    }
                }
            

            // Update active node list (changeIdx) and lineage count appropriately:
            switch (nextEvent.kind) {
                case COALESCE:
                	Node leftChild = nextEvent.node.getLeft();
                    Node rightChild = nextEvent.node.getRight();
                    changeIdx.remove(nextEvent.node);
                    changeIdx.put(leftChild, -1);
                    changeIdx.put(rightChild, -1);
                    break;
                case SAMPLE:
                    changeIdx.remove(nextEvent.node);
                    break;
                case ADD_DEME:
                	demeIndex+=1;
         			break;
     			case REMOVE_DEME:
     				demeIndex+=1;
     				break;
            }
            // Add event to list:
            eventList.add(nextEvent);
        }

        // Reverse event and lineage count lists (order them from tips to root):
        eventList = Lists.reverse(eventList);
        
    	
    	//System.out.println("clearing new likelihoods");
    	for (int i =0; i<mtTree.getNodeCount();i++) pLikesNew[i]= new ArrayList<conciseVector>();
        for (int i =0; i<mtTree.getNodeCount();i++) pDates[i]= new ArrayList<Double>();
    	//System.out.println("Starting updateEventSequenceToSetDemes");
        
        
        //Initialize track of inactive demes
        int eventInd=0;
        List<Integer> inactiveDemes= new ArrayList<Integer>();
        int minD= migrationModelUniform.getMinNumDemes();
        //System.out.println("EventList Size: "+eventList.size());
        for (int i =0;i<minD;i++) inactiveDemes= addSorted(inactiveDemes,i);
        while (eventList.get(eventInd).kind!=SCEventKind.SAMPLE){
        	if (eventList.get(eventInd).kind==SCEventKind.REMOVE_DEME) inactiveDemes= removeElement(inactiveDemes, eventList.get(eventInd).type);
        	if (eventList.get(eventInd).kind==SCEventKind.ADD_DEME) inactiveDemes=addSorted(inactiveDemes, eventList.get(eventInd).type);
        	eventInd+=1;
        }
        
        // Initialise lineage count per colour array:
        SCEvent nextEvent = eventList.get(eventInd);
        double coalTime=nextEvent.time;
        conciseVector lineageCount = new conciseVector(nTypes,nextEvent.type,inactiveDemes);
        
        if ((inactiveDemes.contains(nextEvent.type))) {
        	System.out.println("Deme (" + nextEvent.type + ") is not available to be sampled. Wrong epi/sampling dates?");
        	System.exit(0);
        }
        
        //System.out.println("Initialized demes");
        
        //Initialize first node
        MultiTypeNodeConcise firstNode = (MultiTypeNodeConcise) nextEvent.node;
        //firstNode.setCacheIndex(0);
        //setLikes(lineageCount.copy(),firstNode.getNr());
        //tracking all likelihoods
        setLikesNew(lineageCount.copy(),firstNode.getNr());
        pDates[firstNode.getNr()].add(nextEvent.time);
        //numberList.add(firstNode.getNr());
        Boolean[] isOn = new Boolean[mtTree.getNodeCount()];
        for (int ca = 0; ca<mtTree.getNodeCount(); ca++) isOn[ca]=false;
        isOn[firstNode.getNr()]=true;
        //int[] indListNew=new int[mtTree.getNodeCount()];
        //indListNew[firstNode.getNr()]=0;
        
        //System.out.println("Initialized new parameters");
        
        //Find last coalescent event
        int lastEvent=eventList.size()-1;
        while (eventList.get(lastEvent).kind!=SCEventKind.COALESCE) lastEvent--;
        
        //System.out.println("Starting updateEventSequenceToSetDemes iteration");

        //System.out.println("Start for cycle on events ");
        for (int c = eventInd+ 1; c<=lastEvent; c++) {
	        	nextEvent = eventList.get(c);
	        	//System.out.println("Step 1");
	        	//if ((nextEvent.time != coalTime) || (nextEvent.kind==SCEventKind.COALESCE)){
        		double delta_t = nextEvent.time - coalTime;

        		for (int ca = 0; ca<mtTree.getNodeCount(); ca++) {
        			if (isOn[ca] && delta_t>0.0){
        				conciseVector prob =getLikesNew(ca).get(getLikesNew(ca).size()-1).copy();
        				conciseVector newProb = prob.matrixMul(delta_t*migrationModelUniform.getRate()*(migrationModelUniform.getNumDemes()-1));//*(nTypes-inactiveDemes.size())
            			setLikesNew(newProb.copy(),ca);
            	        pDates[ca].add(nextEvent.time);
            		}
        		}
        		//lineageCountSquareList.add(lineageCountSquare.copy());
        		
				//update counts after the event (not used for present coalescing rates)
        		switch (nextEvent.kind) {
                case COALESCE:
                	//System.out.println("Coalescence ");
                    MultiTypeNodeConcise leftChild = (MultiTypeNodeConcise) nextEvent.node.getLeft();
                    MultiTypeNodeConcise rightChild = (MultiTypeNodeConcise) nextEvent.node.getRight();
                    if (isOn[leftChild.getNr()]==false) System.out.println("Something wrong left ");
                    if (isOn[rightChild.getNr()]==false) System.out.println("Something wrong right ");
                    isOn[leftChild.getNr()]=false;
                    isOn[rightChild.getNr()]=false;
                    //System.out.println("Got sons "+leftChild.getNr()+" "+rightChild.getNr()+" of "+nextEvent.node.getNr());
                    
                    //calculate the probability vector for the new coalesced lineage, following the coalescent event
                    conciseVector v = getLikesNew(leftChild.getNr(),true).copy();//getLikesNew(leftChild.getNr());
                    v=v.entrywiseProd(getLikesNew(rightChild.getNr(),true));
                    //conciseVector v = lineageProbs.get(index1).copy();getLikesNew(leftChild.getNr());
                    //v=v.entrywiseProd(lineageProbs.get(index2));
                    v.scaleV(1.0/migrationModelUniform.getPopSize());
                    double tot= v.sumV();
                    v.scaleV(1.0/tot);
                    setLikesNew(v.copy(),nextEvent.node.getNr());
        	        pDates[nextEvent.node.getNr()].add(nextEvent.time);
        	        isOn[nextEvent.node.getNr()]=true;
                    break;

                case SAMPLE:
                	//System.out.println("Sample "+nextEvent.node.getNr());
                    conciseVector probs = new conciseVector(nTypes, nextEvent.type, inactiveDemes);
        	        //lineageProbs.add(probs.copy());
        	        MultiTypeNodeConcise sampled = (MultiTypeNodeConcise) nextEvent.node;
        	        setLikesNew(probs.copy(), sampled.getNr());
        	        pDates[sampled.getNr()].add(nextEvent.time);
        	        isOn[sampled.getNr()]=true;
                    //sampled.setCacheIndex(lineageProbs.size()-1);
                    break;
                    
                case REMOVE_DEME:
                	//System.out.println("Deme removal "+nextEvent.type);
                	//Adding a deme at a new time point (as it is backward in time, extinction means adding a deme)
                	//for (int t = 0; t<typeCached.size(); t++) typeCached.set(t,-1);
                	inactiveDemes=removeElement(inactiveDemes, (Integer)nextEvent.type);
                    for (int ca = 0; ca<mtTree.getNodeCount(); ca++) {
            			if (isOn[ca]){
            				getLikesNew(ca,true).addDeme(nextEvent.type);
            				//lineageProbs.get(ca).addDeme(nextEvent.type);
            			}
            		}
                    break;
                    
                case ADD_DEME:
                	//System.out.println("Adding deme "+nextEvent.type);
                	//Removing a deme at a new time point (as it is backward in time, extinction means adding a deme)
                	//System.out.println("Trying to record probabilities for likelihood improvement ");
                	//for (int t = 0; t<typeCached.size(); t++) typeCached.set(t,-1);
                	for (int ca = 0; ca<mtTree.getNodeCount(); ca++) {
            			if (isOn[ca]){
            				getLikesNew(ca,true).removeDeme(nextEvent.type);
            				//lineageProbs.get(ca).addDeme(nextEvent.type);
            			}
                	}
                	inactiveDemes=addSorted(inactiveDemes, nextEvent.type);
                    break;
        		}
        	
        	//lineageCountListAfter.add(lineageCount.copy());
            //lineageCountSquareListAfter.add(lineageCountSquare.copy());
        	coalTime=nextEvent.time;
        	//System.out.println("Finished iteration");
        	
        }
        //System.out.println("Ending updateEventSequenceToSetDemes");
    }
    
    

    
    @Override
    public boolean requiresRecalculation() {
        return true;
    }

    /**
     * Test likelihood result. Duplicate of JUnit test for debugging purposes.
     *
     * @param argv
     */
    public static void main(String[] argv) throws Exception {

        // Assemble test MultiTypeTree:
        String newickStr =
                "(((A[state=1]:0.25)[state=0]:0.25,B[state=0]:0.5)[state=0]:1.5,"
                +"(C[state=0]:1.0,D[state=0]:1.0)[state=0]:1.0)[state=0]:0.0;";

        MultiTypeTreeFromNewickVolz mtTree = new MultiTypeTreeFromNewickVolz();
        mtTree.initByName(
                "newick", newickStr,
                "typeLabel", "state",
                "nTypes", 2);

        // Assemble migration model:
        RealParameter rateMatrix = new RealParameter();
        rateMatrix.initByName("value", "2.0 1.0");
        RealParameter popSizes = new RealParameter();
        popSizes.initByName("value", "5.0 10.0");
        MigrationModelUniform migrationModel = new MigrationModelUniform();
        migrationModel.initByName(
                "rateMatrix", rateMatrix,
                "popSizes", popSizes);

        // Set up likelihood instance:
        StructuredCoalescentTreeDensityNew likelihood = new StructuredCoalescentTreeDensityNew();
        likelihood.initByName(
                "migrationModel", migrationModel,
                "multiTypeTree", mtTree);

        double expResult = -16.52831;  // Calculated by hand
        double result = likelihood.calculateLogP();

        System.out.println("Expected result: "+expResult);
        System.out.println("Actual result: "+result);
        System.out.println("Difference: "+String.valueOf(result-expResult));

    }
}