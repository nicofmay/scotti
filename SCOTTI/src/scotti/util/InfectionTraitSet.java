package scotti.util;

//import java.text.SimpleDateFormat;
//import java.util.Date;


import beast.base.core.BEASTObject;
import beast.base.core.Description;
import beast.base.core.Input;
import beast.base.core.Input.Validate;
import scotti.evolution.tree.TraitSetWithLimits;

import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


@Description("An infection trait set represents a collection of epi data of hosts, or, equivalently, demes." +
		"In particular, this is used to set earliest possible infection time and known end of infectivity (such as complete eradication)."+
        "The traits are represented as text content in deme=value form, for example, we" +
        "could have a content of patient1=10,patient2=31. All white space is ignored, so they can" +
        "be put on multiple tabbed lines in the XML.")
public class InfectionTraitSet extends BEASTObject {

    public enum Units {
        year, month, day
    }

    public Input<String> traitNameInput = new Input<String>("traitname", "name of the trait, used as meta data name for the tree. " +
            "Special traitnames that are recognized are '" + DATE_TRAIT + "','" + DATE_FORWARD_TRAIT + "' and '" + DATE_BACKWARD_TRAIT + "'.", Validate.REQUIRED);
    public Input<Units> unitsInput = new Input<Units>("units", "name of the units in which values are posed, " +
            "used for conversion to a real value. This can be " + Arrays.toString(Units.values()) + " (default 'year')", Units.year, Units.values());
    public Input<String> traitsInput = new Input<String>("value", "traits encoded as taxon=value pairs separated by commas", Validate.REQUIRED);
    //public Input<TaxonSet> taxaInput = new Input<TaxonSet>("taxa", "contains list of taxa to map traits to", Validate.REQUIRED);
    public Input<TraitSetWithLimits> taxaDatesInput = new Input<TraitSetWithLimits>("taxaDates", "contains list of taxa dates", Validate.REQUIRED);
    public Input<TraitSetWithLimits> taxaTypesInput = new Input<TraitSetWithLimits>("taxaTypes", "contains list of taxa types", Validate.REQUIRED);
    public Input<String> typeInput = new Input<String>("type", "Is it start or end of infection? Can only be \"start\" or \"end\".", Validate.REQUIRED);

    final public static String DATE_TRAIT = "date";
    final public static String DATE_FORWARD_TRAIT = "date-forward";
    final public static String DATE_BACKWARD_TRAIT = "date-backward";
    
    public String type;
    

    /**
     * String values of demes*
     */
    //String[] taxonValues;
    Map<String, String> demeValues;
    
    TraitSetWithLimits taxaDates;
    TraitSetWithLimits taxaTypes;
    
    /**
     * double representation of taxa value *
     */
    //double[] values;
    Map<String, Double> values;
    double minValue;
    double maxValue;

    /**
     * Whether or not values are ALL numeric.
     */
    boolean numeric = true;
    
    @Override
    public void initAndValidate() throws IllegalArgumentException {
        if (traitsInput.get().matches("^\\s*$")) {
            return;
        }

        // first, determine taxon numbers associated with traits
        // The Taxon number is the index in the alignment, and
        // used as node number in a tree.
        //List<String> labels = taxaInput.get().asStringList();
        taxaDates=taxaDatesInput.get();
        taxaTypes=taxaTypesInput.get();
        
        type=typeInput.get();
        if (!(type.equals("start")) && !(type.equals("end"))) throw new IllegalArgumentException("type of InfectionTraitSet can only be start or end \"" + type+"\"");
        //else System.out.println("Epi data recognized: \"" + type+"\"");
        
        String[] typeArray=taxaTypes.getStringValues();
        
        String[] traits = traitsInput.get().split(",");
        //demeValues = new String[traits.length];
        Map<String, String> demeValues= new HashMap<String, String>();
        //values = new double[traits.length];
        //Map<String, Double> values= new HashMap<String, Double>();
        values= new HashMap<String, Double>();
        for (String trait : traits) {
            trait = trait.replaceAll("\\s+", " ");
            String[] sStrs = trait.split("=");
            if (sStrs.length != 2) {
                throw new IllegalArgumentException("could not parse trait: " + trait);
            }
            String demeID = normalize(sStrs[0]);
            //int taxonNr = labels.indexOf(taxonID);
            if (!(Arrays.asList(typeArray).contains(demeID))) {
                throw new IllegalArgumentException("Trait (" + demeID + ") is not associated to any deme. Spelling error perhaps?");
            }
            demeValues.put(demeID, normalize(sStrs[1]));
            //taxonValues[taxonNr] = normalize(sStrs[1]);
            try {
				values.put(demeID, parseDouble(demeValues.get(demeID)));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            //values[taxonNr] = parseDouble(taxonValues[taxonNr]);
            
            if (Double.isNaN(values.get(demeID)))
                numeric = false;
        }

        // sanity check: did we cover all taxa?
//        for (int i = 0; i < labels.size(); i++) {
//            if (taxonValues[i] == null) {
//                System.out.println("WARNING: no trait specified for " + labels.get(i));
//            }
//        }
        
        minValue=taxaDates.getMinValue();
        maxValue=taxaDates.getMaxValue();

        // find extremes
//        minValue = values[0];
//        maxValue = values[0];
//        for (double fValue : values) {
//            minValue = Math.min(minValue, fValue);
//            maxValue = Math.max(maxValue, fValue);
//        }

        if (traitNameInput.get().equals(DATE_TRAIT) || traitNameInput.get().equals(DATE_FORWARD_TRAIT)) {
        	for (String i : values.keySet()) {
        		values.put(i,maxValue - values.get(i));
        	}
//            for (int i = 0; i < labels.size(); i++) {
//                values[i] = maxValue - values[i];
//            }
        }

        if (traitNameInput.get().equals(DATE_BACKWARD_TRAIT)) {
        	for (String i : values.keySet()) {
        		values.put(i,values.get(i)-minValue);
        	}
//            for (int i = 0; i < labels.size(); i++) {
//                values[i] = values[i] - minValue;
//            }
        }

        
        for (String i : values.keySet()) {
        	System.out.println(i + " = " + demeValues.get(i) + " (" + (values.get(i)) + ")");
        }
//        for (int i = 0; i < labels.size(); i++) {
//            System.out.println(labels.get(i) + " = " + taxonValues[i] + " (" + (values[i]) + ")");
//        }
    } // initAndValidate

    /**
     * some getters and setters *
     */
    public String getTraitName() {
        return traitNameInput.get();
    }

    public String getStringValue(String i) {
        return demeValues.get(i);
    }

    public double getValue(String i) {
        if (values == null) {
            return 0;
        }
        return values.get(i);
    }
    
    public Map<String, Double> getValues() {
        return values;
    }

    /**
     * see if we can convert the string to a double value *
     */
    private double parseDouble(String sStr) throws Exception {
        // default, try to interpret the string as a number
        try {
            return Double.parseDouble(sStr);
        } catch (NumberFormatException e) {
            // does not look like a number
            if (traitNameInput.get().equals(DATE_TRAIT) || 
            	traitNameInput.get().equals(DATE_FORWARD_TRAIT) ||
            	traitNameInput.get().equals(DATE_BACKWARD_TRAIT))	{
            	try {
            		if (sStr.matches(".*[a-zA-Z].*")) {
            			sStr = sStr.replace('/', '-');
            		}
            		long date = Date.parse(sStr);
            		double year = 1970.0 + date / (60.0*60*24*365*1000);
            		switch (unitsInput.get()) {
            		case month : return year * 12.0;
            		case day: return year * 365;
            		default :
            			return year;
            		}
            	} catch (Exception e2) {
            		// does not look like a date, give up
    			}
            }
        }
        //return 0;
        return Double.NaN;
    } // parseStrings

    /**
     * remove start and end spaces
     */
    String normalize(String sStr) {
        if (sStr.charAt(0) == ' ') {
            sStr = sStr.substring(1);
        }
        if (sStr.endsWith(" ")) {
            sStr = sStr.substring(0, sStr.length() - 1);
        }
        return sStr;
    }

    public double getDate(double fHeight) {
        if (traitNameInput.get().equals(DATE_TRAIT) || traitNameInput.get().equals(DATE_FORWARD_TRAIT)) {
            return maxValue - fHeight;
        }

        if (traitNameInput.get().equals(DATE_BACKWARD_TRAIT)) {
            return minValue + fHeight;
        }
        return fHeight;
    }
    
    /**
     * Determines whether trait is recognised as specifying taxa dates.
     * @return true if this is a date trait.
     */
    public boolean isDateTrait() {
        return traitNameInput.get().equals(DATE_TRAIT)
                || traitNameInput.get().equals(DATE_FORWARD_TRAIT)
                || traitNameInput.get().equals(DATE_BACKWARD_TRAIT);
    }
    

    /**
     * @return true if trait values are (all) numeric.
     */
    public boolean isNumeric() {
        return numeric;
    }
    
} // class TraitSet
