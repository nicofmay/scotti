/*
 * Copyright (C) 2015 Nicola De Maio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package scotti.evolution.tree;


import beast.base.core.Description;
import beast.base.core.Input;
import beast.base.core.Input.Validate;
import beast.base.evolution.tree.Node;
import beast.base.evolution.tree.Tree;
import beast.base.evolution.tree.TreeParser;
import beast.base.inference.StateNode;
import beast.base.inference.StateNodeInitialiser;
import beast.base.inference.parameter.IntegerParameter;
import beast.base.inference.parameter.RealParameter;
import beast.base.util.DiscreteStatistics;
import beast.base.util.Randomizer;
import com.google.common.collect.Lists;

import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.*;
//import java.util.Random;

//import multitypetreeVolz.distributions.StructuredCoalescentTreeDensityConcise.demeEvent;

//import multitypetreeVolz.distributions.StructuredCoalescentTreeDensityConcise;
 
/**
 * A Volz-type tree generated randomly from leaf types, a number of demes, a migration rate, and a common effective population size.
 *
 * @author Nicola De Maio
 */
@Description("A Volz-type tree generated randomly from leaf types, a number of demes, a migration rate, and a common effective population size.")
public class StructuredCoalescentMultiTypeTreeConcise extends MultiTypeTreeConcise implements StateNodeInitialiser {

    /*
     * Plugin inputs:
     */
    public Input<MigrationModelUniform> migrationModelInput = new Input<MigrationModelUniform>(
            "migrationModelUniform",
            "Migration model to use in simulator.",
            Validate.REQUIRED);
    
    public Input<IntegerParameter> leafTypesInput = new Input<IntegerParameter>(
            "leafTypes",
            "Types of leaf nodes.");
    
    public Input<String> outputFileNameInput = new Input<String>(
            "outputFileName", "Optional name of file to write simulated "
                    + "tree to.");
    
    //public Input<Boolean> useNewickInput = new Input<Boolean>("useNewick", "Use tree in Newick format as starting tree? Default false.", false);
    
   public Input<String> newickStartInput = new Input<String>("newickStart", "Tree in Newick format.", "");

    /*
     * Non-input fields:
     */
    
    //public int nTypes=migrationModelInput.get().getNumDemes();
    protected MigrationModelUniform migrationModelUniform;
    
   // private Random randomGenerator;
    
    private List<Integer> leafTypes;
    private List<String> leafNames;
    private List<Double> leafTimes;
    private int nLeaves;
    private String newick;
    
    public class demeEvent {
  	  public Integer index;
  	  public Double time;
  	  public String type;

  	  public demeEvent(Integer left, Double right, String S) {
  	    this.index = left;
  	    this.time = right;
  	    this.type = S;
  	  }
  }
    
    public List<demeEvent> demeEventsList;

    /*
     * Other private fields and classes:
     */
    private abstract class SCEvent {

        double time;
        int fromType, toType;
        //boolean isCoalescence;
        boolean isDeme;
    }

    private class CoalescenceEvent extends SCEvent {

        public CoalescenceEvent(int type, double time) {
            this.fromType = type;
            this.time = time;
            //this.isCoalescence=true;
            this.isDeme=false;
        }
        
    }

    private class MigrationEvent extends SCEvent {

        public MigrationEvent(int fromType, int toType, double time) {
            this.fromType = fromType;
            this.toType = toType;
            this.time = time;
            //this.isCoalescence=false;
            this.isDeme=false;
        }
    }
    
    private class NullEvent extends SCEvent {
        public NullEvent() {
            this.time = Double.POSITIVE_INFINITY;
            //this.isCoalescence=false;
            this.isDeme=false;
        }
    }
    
    private class DemeOpenEvent extends SCEvent {

        public DemeOpenEvent(demeEvent eD) {
            this.fromType = eD.index;
            this.time = eD.time;
            //this.isCoalescence=false;
            this.isDeme=true;
        }
    }
    
    private class DemeCloseEvent extends SCEvent {

        public DemeCloseEvent(demeEvent eD) {
            this.fromType = eD.index;
            this.time = eD.time;
            //this.isCoalescence=false;
            this.isDeme=true;
        }
    }
    
    public StructuredCoalescentMultiTypeTreeConcise() { }

    @Override
    public void initAndValidate() throws IllegalArgumentException {
    	
    	//System.out.printf("Initializing simulation\n");
    	
        super.initAndValidate();
        
        //System.out.printf("Initializing simulation 2\n");

        // Obtain required parameters from inputs:
        migrationModelUniform = migrationModelInput.get();
        newick = newickStartInput.get();
        nTypes = migrationModelUniform.getNumDemes();

        // Obtain leaf colours from explicit input or alignment:
        leafTypes = Lists.newArrayList();
        leafNames = Lists.newArrayList();
        
        //System.out.printf("Started init\n");
        if (leafTypesInput.get() != null) {            
            for (int i=0; i<leafTypesInput.get().getDimension(); i++) {
                leafTypes.add(leafTypesInput.get().getValue(i));
                leafNames.add(String.valueOf(i));
            }
        } else {
            if (!hasTypeTrait())
                throw new IllegalArgumentException("Either leafColours or "
                        + "trait set (with name '" + typeLabel
                        + "') must be provided.");

            // Fill leaf colour array:
            for (int i = 0; i<typeTraitSet.taxaInput.get().asStringList().size(); i++) {
                leafTypes.add(getTypeList().indexOf(typeTraitSet.getStringValue(i)));
                leafNames.add(typeTraitSet.taxaInput.get().asStringList().get(i));
            }
        }
        
        //System.out.printf("Initializing simulation 3\n");
        
        nLeaves = leafTypes.size();
        
        // Set leaf times if specified:
        leafTimes = Lists.newArrayList();
        if (timeTraitSet == null) {
            for (int i=0; i<nLeaves; i++)
                leafTimes.add(0.0);
            System.out.println("No time trait in tree.");
        } else {
        	System.out.println("Found time trait in tree.");
            if (timeTraitSet.taxaInput.get().asStringList().size() != nLeaves)
                throw new IllegalArgumentException("Number of time traits "
                        + "doesn't match number of leaf colours supplied.");
            
            for (int i=0; i<nLeaves; i++)
                leafTimes.add(timeTraitSet.getValue(i));
        }
        
        //System.out.printf("Initializing simulation 3.5\n");
        
        //create list of demes openings and closures
        demeEvent[] demeEvents= new demeEvent[migrationModelUniform.getMinNumDemes()*2];
        //endValues= new Double[migrationModelUniform.getMinNumDemes()];
        for (int i=0; i<migrationModelUniform.getMinNumDemes();i++){
        	demeEvents[2*i]=new demeEvent(i,Double.POSITIVE_INFINITY,"start");
        	demeEvents[2*i+1]=new demeEvent(i,Double.NEGATIVE_INFINITY,"end");
        }
        //System.out.printf("Initializing simulation 3.6\n");
        //System.out.printf("startValues : "+migrationModelUniform.startValues+"\n");
        if (migrationModelUniform.startValues!=null){
	        for(String s : migrationModelUniform.startValues.keySet()){
	        	//System.out.printf("inside for\n");
	        	int i=getTypeList().indexOf(s);
	        	//System.out.printf("Index "+i+"\n");
	        	//startValues[i]=migrationModelUniform.startValues.get(s);
	        	demeEvent de = new demeEvent(i,migrationModelUniform.startValues.get(s),"start");
	        	demeEvents[2*i]=de;
	        }
        }
        //System.out.printf("Initializing simulation 3.7\n");
        if (migrationModelUniform.endValues!=null){
	        for(String s : migrationModelUniform.endValues.keySet()){
	        	int i=getTypeList().indexOf(s);
	        	//endValues[i]=migrationModelUniform.endValues.get(s);
	        	demeEvent de = new demeEvent(i,migrationModelUniform.endValues.get(s),"end");
	        	demeEvents[2*i+1]=de;
	        }
        }
        //System.out.printf("Initializing simulation 3.8\n");
        demeEventsList=Arrays.asList(demeEvents);
        Collections.sort(demeEventsList, new Comparator<demeEvent>() {
            public int compare(demeEvent o1, demeEvent o2) {
                return o2.time.compareTo(o1.time);
            }
        });
        //demeEventsList=Lists.reverse(demeEventsList);
        Collections.reverse(demeEventsList);
        //List<String> reverseView = Lists.reverse(letters); 
        //System.out.printf("Initializing simulation , first deme event:"+demeEventsList.get(0).time+"\n");
        
        //System.out.printf("Initializing simulation 4\n");
        
//        try {
//			assignFromWithoutID(new MultiTypeTreeConcise(simulateTree()));
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//////////////////////////////////////////
        // Construct tree and assign to input plugin:
        if (newickStartInput.get()==""){
        	try {
    			assignFromWithoutID(new MultiTypeTreeConcise(simulateTree()));
    		} catch (Exception e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
        }
        else{
        	//Use input tree
        	TreeParser parser = new TreeParser();
//        	parser.timeTraitSet=this.timeTraitSet;
            parser.setDateTrait(this.timeTraitSet);
//        	Double[] vs= new Double[this.typeTraitSet.values.length];
//        	for (int i=0;i<this.typeTraitSet.values.length;i++) {
//        		vs[i]=this.typeTraitSet.values[i];
//        		System.out.println("for node number "+i+" state "+vs[i]);
//        	}
        	
        	//parser.setMetaData(parser.root, vs, typeLabel);
        	//parser.typeTraitSet=this.typeTraitSet;
            parser.initByName(
                    "IsLabelledNewick", true,
                    "adjustTipHeights", false,
                    "singlechild", true,
                    "newick", newickStartInput.get());
            Tree tree = parser;
            try {
            	initFromNormalTree(tree, true, this.typeTraitSet, typeLabel);
            } catch (Exception e) {
            	System.out.printf("Error initializing tree\n");
            	System.exit(0);
            }
            initArrays();
        }
///////////////////////////////////////////
        
        //System.out.printf("Initializing simulation 5\n");

        // Write tree to disk if requested
        if (outputFileNameInput.get() != null) {
        	//System.out.printf("output stream?\n");
            PrintStream pstream;
			try {
				pstream = new PrintStream(outputFileNameInput.get());
				pstream.println("#nexus\nbegin trees;");
	            pstream.println("tree TREE_1 = " + toString() + ";");
	            pstream.println("end;");
	            
	            pstream.close();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
        }
        //System.out.printf("Init done\n");
        
        
    }

    /**
     * Generates tree using the specified list of active leaf nodes using the
     * structured coalescent.
     *
//     * @param activeNodes
     * @return Root node of generated tree.
     */
    private MultiTypeNodeConcise simulateTree() throws Exception {    //USE TREE IN NEWICK IF AVAILABLE!!!!!!!!
    	
    	//System.out.printf("Start simulating\n");
        // Initialise node creation counter:
        int nextNodeNr = 0;

        // Initialise node lists:
        conciseVectorNodeList activeNodes = new conciseVectorNodeList(migrationModelUniform.getNumDemes());
        conciseVectorNodeList inactiveNodes = new conciseVectorNodeList(migrationModelUniform.getNumDemes());
        //List<MultiTypeNodeVolz> inactiveNodes = Lists.newArrayList();
        //conciseNodeList
        //List<List<MultiTypeNodeVolz>> CNActiveList = Lists.newArrayList();
        //List<Integer> CNActiveIndeces = Lists.newArrayList();
        //List<List<MultiTypeNodeVolz>> CNActiveList = Lists.newArrayList();
        //List<Integer> CNActiveIndeces = Lists.newArrayList();
        //for (int i = 0; i < nTypes; i++) {
        //    activeNodes.add(new ArrayList<MultiTypeNodeVolz>());
        //    inactiveNodes.add(new ArrayList<MultiTypeNodeVolz>());
        //}
        
        //System.out.println("add nodes to lists");

        // Add nodes to inactive nodes list:
        for (int l = 0; l < nLeaves; l++) {
            MultiTypeNodeConcise node = new MultiTypeNodeConcise();
            node.setNr(nextNodeNr);
            node.setID(leafNames.get(l));
            //inactiveNodes.add(node);
            node.setHeight(leafTimes.get(l));
            node.setNodeType(leafTypes.get(l));
            inactiveNodes.add(leafTypes.get(l),node);

            nextNodeNr++;
        }
        
        //System.out.println("sort nodes per age");
        
        // Sort nodes in inactive nodes lists in order of increasing age:
        for (int i=0; i<inactiveNodes.diffValues.size(); i++) {
	        Collections.sort(inactiveNodes.diffValues.get(i), new Comparator<MultiTypeNodeConcise>() {
	            @Override
	            public int compare(MultiTypeNodeConcise node1, MultiTypeNodeConcise node2) {
	                double dt = node1.getHeight()-node2.getHeight();
	                if (dt<0)
	                    return -1;
	                if (dt>0)
	                    return 1;
	                
	                return 0;
	            }
	        });
        
        
        }
        
        
        //List of number of lineages in demes, without 0s
        conciseVectorInt numLinInDemes = new conciseVectorInt(nTypes);

        double t = 0.0;
        
        //System.out.println("Start iteration over events");
        
        List<Integer> activeDemes = new ArrayList<Integer>();
        
        int demeEventIndex=0;

        while ((activeNodes.sum())>1
                || (inactiveNodes.sum())>0) {
        	//System.out.printf("New simulation step "+activeNodes.diffIndeces+" "+inactiveNodes.diffIndeces+"\n");
            // Step 1: Calculate propensities.
        	double totalProp=updatePropensities(numLinInDemes,activeDemes.size()+nTypes-migrationModelUniform.getMinNumDemes());;
        	//System.out.printf("ActiveDemes: "+activeDemes+" propensity "+totalProp+" t "+t+" \n");
        	
            // Step 2: Determine next event.
        	//System.out.printf("Get event "+demeEventsList.size()+" "+demeEventIndex+" "+demeEventsList.get(demeEventIndex).time+"\n");
            SCEvent event = getNextEvent( numLinInDemes, totalProp, t,demeEventsList.get(demeEventIndex), activeDemes);
            //System.out.printf("Got event, is deme: "+event.isDeme+" time: "+event.time+"\n");
            
            
            // Step 3: Handle activation of nodes:
            MultiTypeNodeConcise nextNode = null ;
            int nextNodeType = -1;
            double nextTime = Double.POSITIVE_INFINITY;
            //int nextNodeTypeIndex = -1;
            for (int i=0; i<inactiveNodes.diffValues.size(); i++) {
            	//System.out.printf("Number inactive nodes in "+inactiveNodes.diffIndeces.get(i)+" is "+inactiveNodes.diffValues.get(i).size()+"\n");
                if (inactiveNodes.diffValues.get(i).isEmpty())
                    continue;
                
                if (inactiveNodes.diffValues.get(i).get(0).getHeight()<nextTime) {
                    nextNode = inactiveNodes.diffValues.get(i).get(0);
                    nextTime = nextNode.getHeight();
                    nextNodeType = inactiveNodes.diffIndeces.get(i);
                    //nextNodeTypeIndex = inactiveNodes.diffIndeces.get(i);
                }
            }
            if (nextTime < event.time) {
                t = nextTime;
                activeNodes.add(nextNodeType, nextNode);
                inactiveNodes.remove(nextNodeType);
                numLinInDemes.add(nextNodeType, 1);
//                inactiveNodes.diffValues.get(nextNodeTypeIndex).remove(0);
//                if (inactiveNodes.diffValues.get(nextNodeTypeIndex).isEmpty()){
//                	inactiveNodes.diffValues.remove(nextNodeTypeIndex);
//                	inactiveNodes.diffIndeces.remove(nextNodeTypeIndex);
//                }
                //System.out.printf("Add sample\n");
                continue;
            }
            
            if (event.isDeme) demeEventIndex++;
            
            // Step 4: Place event on tree.
            nextNodeNr = updateTree(activeNodes,numLinInDemes, event, nextNodeNr, activeDemes);
//            for (int i=0; i<activeNodes.diffValues.size(); i++) {
//            	System.out.printf("Number active nodes in "+activeNodes.diffIndeces.get(i)+" is "+activeNodes.diffValues.get(i).size()+"\n");
//            }
            //System.out.printf("NumLinInDemes "+numLinInDemes.diffValues+" "+numLinInDemes.diffIndeces+" \n");
            
            // Step 5: Keep track of time increment.
            if (event.time>t) t = event.time;
        }
        
        //System.out.printf("Done simulating\n");
        // Return sole remaining active node as root:
        for (List<MultiTypeNodeConcise> nodeList : activeNodes.diffValues)
            if (!nodeList.isEmpty())
                return nodeList.get(0);
        //return activeNodes.get(0);

        // Should not fall through.
        throw new Exception("No active nodes remaining end of "
                + "structured coalescent simulation!");
    }

    /**
     * Obtain propensities (instantaneous reaction rates) for coalescence and
     * migration events.
     *
//     * @param migrationProp
//     * @param coalesceProp
//     * @param activeNodes
     * @return Total reaction propensity.
     */
    private double updatePropensities(conciseVectorInt numLinInDemes, int numActiveDemes) {
    	//System.out.printf("Start updating propensities\n");
        double totalProp = 0.0;
        //int migrationPropSize=activeNodes.size();
        //totalProp+=migrationPropSize*(migrationPropSize-1)*migrationModelUniform.getRate();
        double rate=migrationModelUniform.getRate();
        //int sum=numLinInDemes.sumV();
        
        double N = migrationModelUniform.getPopSize();
        for (int i = 0; i < numLinInDemes.diffIndeces.size(); i++) {

           
            int k = numLinInDemes.diffValues.get(i);

            if (k>1) totalProp += k * (k - 1) / (2.0 * N);
            
            //totalProp += k * (numLinInDemes.numDemes-1) * rate;
            totalProp += k * (numActiveDemes-1) * rate;
        }
        //System.out.printf("End updating propensities "+totalProp+"\n");
        return totalProp;

    }

    /**
     * Obtain type and location of next reaction.
     *
//     * @param migrateProp Current migration propensities.
//     * @param coalesceProp Current coalescence propensities.
     * @param t Current time.
     * @return Event object describing next event.
     */
    private SCEvent getNextEvent(conciseVectorInt numLinInDemes, double totalProp, double t, demeEvent dE, List<Integer> activeDemes)
            throws Exception {
    	
    	
        
        if (t>dE.time) {
        	//System.out.printf("Deme event\n");
        	if (dE.type == "start") return new DemeOpenEvent(dE);
        	if (dE.type == "end") return new DemeCloseEvent(dE);
        }
        
      //System.out.printf("Start sampling event "+totalProp+" "+numLinInDemes.diffValues+" "+numLinInDemes.diffIndeces+" \n");
        // Get time of next event:
    	if (totalProp>0.0)
            t += Randomizer.nextExponential(totalProp);
        else{
        	//System.out.printf("End sampling event - null event\n");
        	t=0.0;
            return new NullEvent();
        }
    	
    	if (t>dE.time) {
        	//System.out.printf("Deme event\n");
        	if (dE.type == "start") return new DemeOpenEvent(dE);
        	if (dE.type == "end") return new DemeCloseEvent(dE);
        }
    	

        // Select event type:
        double U = Randomizer.nextDouble() * totalProp;
        double N = migrationModelUniform.getPopSize();
        double migrateProp=0.0;
        //Random rand = new Random();
        int r;
        int nTypes=numLinInDemes.numDemes;

        for (int i = 0; i < numLinInDemes.diffIndeces.size(); i++) {
        	double coalProp=0.0;
        	if (numLinInDemes.diffValues.get(i)>1) coalProp+=(numLinInDemes.diffValues.get(i)*(numLinInDemes.diffValues.get(i)-1))/(2.0 * N);
        	//System.out.printf("CoalProp: "+coalProp+"\n");

            if (U < coalProp){
            	//System.out.printf("End sampling event - coalescent event "+numLinInDemes.diffIndeces.get(i)+" \n");
            	return new CoalescenceEvent(numLinInDemes.diffIndeces.get(i), t);
            }
            else
                U -= coalProp;
            
            //int aDSize=activeDemes.size();
            //if (activeDemes==null) aDSize=0;
            //else aDSize=activeDemes.size();
            migrateProp=numLinInDemes.diffValues.get(i)*((activeDemes.size()+nTypes-migrationModelUniform.getMinNumDemes())-1)*migrationModelUniform.getRate();
            if (U < migrateProp){
            	do r= Randomizer.nextInt(nTypes); while ((r==numLinInDemes.diffIndeces.get(i)) || !(activeDemes.contains(r)));
            	//System.out.printf("End sampling event - migration "+numLinInDemes.diffIndeces.get(i)+"\n");
                return new MigrationEvent(numLinInDemes.diffIndeces.get(i),r, t);
            }
            else U -= migrateProp;
        }

        // Loop should not fall through.
        throw new Exception("Structured coalescenct event selection error.");

    }

    /**
     * Update tree with result of latest event.
     *
     * @param activeNodes
     * @param event
     * @param nextNodeNr Integer identifier of last node added to tree.
     * @return Updated nextNodeNr.
     */
    private int updateTree(conciseVectorNodeList activeNodes, conciseVectorInt numLinInDemes, SCEvent event,  int nextNodeNr, List<Integer> activeDemes) {
    	
    	//System.out.println("Start Update tree");
//    	int deme=event.fromType;
//    	List<MultiTypeNodeVolz> demesIn= new ArrayList<MultiTypeNodeVolz>();
//    	for (int i=0;i<activeNodes.size();i++){
//    		if (activeNodes.get(i).nodeType==deme) demesIn.add(activeNodes.get(i));
//    	}
    	int type= event.fromType;
    	int index=-1;
    	List<MultiTypeNodeConcise> demesIn=null;
    	for (int i=0;i<activeNodes.diffIndeces.size();i++)
    		if (activeNodes.diffIndeces.get(i)==type) {
    			index=i;
    			demesIn = activeNodes.diffValues.get(i);
    		}
    	
        if (event instanceof CoalescenceEvent) {
        	//System.out.printf("Coalescent event\n");
        	
        	//System.out.println("Coalescent");
            // Randomly select node pair with chosen colour:
            MultiTypeNodeConcise daughter = selectRandomNode(demesIn);
            MultiTypeNodeConcise son = selectRandomSibling(demesIn, daughter);

            // Create new parent node with appropriate ID and time:
            MultiTypeNodeConcise parent = new MultiTypeNodeConcise();
            parent.setNr(nextNodeNr);
            parent.setID(String.valueOf(nextNodeNr));
            parent.setHeight(event.time);
            nextNodeNr++;

            // Connect new parent to children:
            parent.setLeft(daughter);
            parent.setRight(son);
            son.setParent(parent);
            daughter.setParent(parent);

            // Ensure new parent is set to correct colour:
            parent.setNodeType(event.fromType);

            // Update activeNodes:
            activeNodes.diffValues.get(index).remove(son);
            int idx = activeNodes.diffValues.get(index).indexOf(daughter);
            activeNodes.diffValues.get(index).set(idx, parent);
            
            numLinInDemes.add(event.fromType,-1);

        } else if (event instanceof MigrationEvent) {
        	//System.out.printf("Migration event\n");
        	//int index2=-1;
//        	for (int i=0;i<activeNodes.diffIndeces.size();i++)
//    	    	if (activeNodes.diffIndeces.get(i)==event.toType) {
//    				index2=i;
//    			}
        	
        	//System.out.println("migration from "+event.fromType+" to "+event.toType+"    demes nums: "+demesIn.size() +" "+activeNodes.size());
        	//System.out.println("numLinInDemes: "+numLinInDemes.diffIndeces+numLinInDemes.diffValues+numLinInDemes.numDemes);
            // Randomly select node with chosen colour:
            MultiTypeNodeConcise migrator = selectRandomNode(demesIn);

            // Record colour change in change lists:
            //migrator.addChange(event.toType, event.time);

            // Update activeNodes:
            //migrator.setNodeType(event.toType);   //this is wrong? do not change node types, but create a conciseVector with pointers to the nodes!!!!!!!!!!!!!!!
            numLinInDemes.add(event.fromType,-1);
            numLinInDemes.add(event.toType,1);
            //System.out.println("migration change: "+numLinInDemes.diffIndeces+numLinInDemes.diffValues+numLinInDemes.numDemes);
            activeNodes.diffValues.get(index).remove(migrator);
            if (activeNodes.diffValues.get(index).isEmpty()) {
            	activeNodes.diffIndeces.remove(index);
            	activeNodes.diffValues.remove(index);
            }
            activeNodes.add(event.toType,migrator);

        } else if (event instanceof DemeOpenEvent) {
        	//System.out.printf("Deme opening\n");
        	activeDemes.remove((Integer)event.fromType);
        	//int index2=-1;
//        	for (int i=0;i<activeNodes.diffIndeces.size();i++)
//    	    	if (activeNodes.diffIndeces.get(i)==event.toType) {
//    				index2=i;
//    			}
        	
        	//System.out.println("migration from "+event.fromType+" to "+event.toType+"    demes nums: "+demesIn.size() +" "+activeNodes.size());
        	//System.out.println("numLinInDemes: "+numLinInDemes.diffIndeces+numLinInDemes.diffValues+numLinInDemes.numDemes);
            // Randomly select node with chosen colour:
        	for (int i=0; i<demesIn.size();i++){
        		MultiTypeNodeConcise migrator = demesIn.get(i);
        		int ind = Randomizer.nextInt(activeDemes.size());
        		int demeTo = activeDemes.get(ind);
        		numLinInDemes.add(event.fromType,-1);
                numLinInDemes.add(demeTo,1);
                
                activeNodes.diffValues.get(index).remove(migrator);
                activeNodes.add(demeTo,migrator);
        	}
        	activeNodes.diffIndeces.remove(index);
        	activeNodes.diffValues.remove(index);
            //MultiTypeNodeConcise migrator = selectRandomNode(demesIn);

            // Record colour change in change lists:
            //migrator.addChange(event.toType, event.time);

            // Update activeNodes:
            //migrator.setNodeType(event.toType);   //this is wrong? do not change node types, but create a conciseVector with pointers to the nodes!!!!!!!!!!!!!!!
            //numLinInDemes.add(event.fromType,-1);
            //numLinInDemes.add(event.toType,1);
            //System.out.println("migration change: "+numLinInDemes.diffIndeces+numLinInDemes.diffValues+numLinInDemes.numDemes);
//            activeNodes.diffValues.get(index).remove(migrator);
//            if (activeNodes.diffValues.get(index).isEmpty()) {
//            	activeNodes.diffIndeces.remove(index);
//            	activeNodes.diffValues.remove(index);
//            }
//            activeNodes.add(event.toType,migrator);

        } else if (event instanceof DemeCloseEvent) {
        	//System.out.printf("Deme closing\n");
        	activeDemes.add((Integer)event.fromType);
        }
        //System.out.printf("End Update Tree, new active Demes size: "+activeDemes.size()+"\n");

        return nextNodeNr;

    }

    /**
     * Use beast RNG to select random node from list.
     *
     * @param nodeList
     * @return A randomly selected node.
     */
    private MultiTypeNodeConcise selectRandomNode(List<MultiTypeNodeConcise> nodeList) {
        return nodeList.get(Randomizer.nextInt(nodeList.size()));
    }

    /**
     * Return random node from list, excluding given node.
     *
     * @param nodeList
     * @param node
     * @return Randomly selected node.
     */
    private MultiTypeNodeConcise selectRandomSibling(List<MultiTypeNodeConcise> nodeList, Node node) {

        int n = Randomizer.nextInt(nodeList.size() - 1);
        int idxToAvoid = nodeList.indexOf(node);
        if (n >= idxToAvoid)
            n++;

        return nodeList.get(n);
    }
    
    @Override
    public void initStateNodes() throws IllegalArgumentException { }

    @Override
    public void getInitialisedStateNodes(List<StateNode> stateNodeList) {
        stateNodeList.add(this);
    }
    
    /**
     * Generates an ensemble of trees from the structured coalescent for testing
     * coloured tree-space samplers.
     *
     * @param argv
     */
    public static void main(String[] argv) throws Exception {
    	
        // Set up migration model.
        RealParameter rateMatrix = new RealParameter();
        rateMatrix.initByName(
                "value", "0.05",
                "dimension", "12");
        RealParameter popSizes = new RealParameter();
        popSizes.initByName(
                "value", "7.0",
                "dimension", "4");
        MigrationModelUniform migrationModel = new MigrationModelUniform();
        migrationModel.initByName(
                "rateMatrix", rateMatrix,
                "popSizes", popSizes);

        // Specify leaf types:
        IntegerParameter leafTypes = new IntegerParameter();
        leafTypes.initByName(
                "value", "0 0 0");

        // Generate ensemble:
        int reps = 1000000;
        double[] heights = new double[reps];
        double[] changes = new double[reps];

        long startTime = System.currentTimeMillis();
        StructuredCoalescentMultiTypeTreeConcise sctree;
        sctree = new StructuredCoalescentMultiTypeTreeConcise();
        for (int i = 0; i < reps; i++) {

            if (i % 1000 == 0)
                System.out.format("%d reps done\n", i);

            sctree.initByName(
                    "migrationModel", migrationModel,
                    "leafTypes", leafTypes,
                    "nTypes", 4);

            heights[i] = sctree.getRoot().getHeight();
            //changes[i] = sctree.getTotalNumberOfChanges();
        }

        long time = System.currentTimeMillis() - startTime;

        System.out.printf("E[T] = %1.4f +/- %1.4f\n",
                DiscreteStatistics.mean(heights), DiscreteStatistics.stdev(heights) / Math.sqrt(reps));
        System.out.printf("V[T] = %1.4f\n", DiscreteStatistics.variance(heights));

        //System.out.printf("E[C] = %1.4f +/- %1.4f\n",
        //        DiscreteStatistics.mean(changes), DiscreteStatistics.stdev(changes) / Math.sqrt(reps));
        //System.out.printf("V[C] = %1.4f\n", DiscreteStatistics.variance(changes));

        System.out.printf("Took %1.2f seconds\n", time / 1000.0);

        PrintStream outStream = new PrintStream("heights.txt");
        outStream.println("h c");
        for (int i = 0; i < reps; i++)
            outStream.format("%g %g\n", heights[i], changes[i]);
        outStream.close();
    }

}
