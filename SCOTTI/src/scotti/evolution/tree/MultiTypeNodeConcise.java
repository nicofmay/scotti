/*
 * Copyright (C) 2014 Nicola De Maio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package scotti.evolution.tree;


import beast.base.core.Description;
import beast.base.evolution.tree.Node;

//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.List;

/**
 *
 * @author Nicola De Maio
 */
@Description("A node in a Volz-type phylogenetic tree.")
public class MultiTypeNodeConcise extends Node {
	
	//int migCount = -100;
    // Type metadata:
    int nodeType = -1;
    int cacheIndex = -1;
    //Double[] likes = null;
    
    /**
     * @return position of the lineage in the probability calculation cache
     */
    public int getCacheIndex() {
        return cacheIndex;
    }
    
    /**
     * Sets the index of the node in the cache
     *
     * @param cacheIndex New cache index.
     */
    public void setCacheIndex(int cacheIndex) {
        //startEditing();
        this.cacheIndex = cacheIndex;
    }
    
    /**
     * Obtain type value at this node.
     *
     * @return type value at this node
     */
    public int getNodeType() {
        return nodeType;
    }

    /**
     * Sets type of node.
     *
     * @param nodeType New node type.
     */
    public void setNodeType(int nodeType) {
        //startEditing();
        this.nodeType = nodeType;
    }
    
//	/**
//     * @return number of migration events on branch above node
//     */
//    public int getMigCount() {
//        return migCount;
//    }
    
//    /**
//     * Sets the number of migration events on branch above node
//     */
//    public void setMigCount(int migCount) {
//        //startEditing();
//        this.migCount = migCount;
//    }
    
    /**
     * Remove all type changes from branch above node.
     */
    public void clearChanges() {
        startEditing();
    }


    /**
     * @return shallow copy of node
     */
    public MultiTypeNodeConcise shallowCopy() {
        MultiTypeNodeConcise node = new MultiTypeNodeConcise();
        node.height = height;
        node.parent = parent;        
        node.children.addAll(children);

        node.nodeType = nodeType;
        //node.migCount = migCount;
        node.cacheIndex = cacheIndex;
                
        node.labelNr = labelNr;
        node.metaDataString = metaDataString;
        node.ID = ID;

        return node;
    }
    
    /**
     * **************************
     * Methods ported from Node *
     ***************************
     */

    
    /**
     * methods from Node.java modified to include type structure
     */
    
    
    /**
     * @return (deep) copy of node
     */
    @Override
    public MultiTypeNodeConcise copy() {
        MultiTypeNodeConcise node = new MultiTypeNodeConcise();
        node.height = height;
        node.labelNr = labelNr;
        node.metaDataString = metaDataString;
        node.parent = null;
        node.ID = ID;
        node.cacheIndex = cacheIndex;
        node.nodeType = nodeType;
        //node.migCount = migCount;
        
        if (getLeft()!=null) {
            node.setLeft(getLeft().copy());
            node.getLeft().setParent(node);
            if (getRight()!=null) {
                node.setRight(getRight().copy());
                node.getRight().setParent(node);
            }
        }
        
        return node;
    }

    /**
     * assign values from a tree in array representation *
     * @param nodes
     * @param node
     */
    @Override
    public void assignFrom(Node[] nodes, Node node) {
        height = node.getHeight();
        labelNr = node.getNr();
        metaDataString = node.metaDataString;
        parent = null;
        ID = node.getID();
        
        MultiTypeNodeConcise mtNode = (MultiTypeNodeConcise)node;
        cacheIndex = mtNode.cacheIndex;
        nodeType = mtNode.nodeType;
        //migCount = mtNode.migCount;
        
        if (node.getLeft()!=null) {
            setLeft(nodes[node.getLeft().getNr()]);
            getLeft().assignFrom(nodes, node.getLeft());
            getLeft().setParent(this);
            if (node.getRight()!=null) {
                setRight(nodes[node.getRight().getNr()]);
                getRight().assignFrom(nodes, node.getRight());
                getRight().setParent(this);
            }
        }
    }
}
