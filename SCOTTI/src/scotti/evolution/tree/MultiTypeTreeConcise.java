/*
 * Copyright (C) 2014 Nicola De Maio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package scotti.evolution.tree;

import beast.base.core.Description;
import beast.base.core.Input;
import beast.base.evolution.tree.Node;
import beast.base.evolution.tree.TraitSet;
import beast.base.evolution.tree.Tree;
import beast.base.inference.StateNode;
import beast.base.inference.StateNodeInitialiser;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
//import java.util.logging.Level;
//import java.util.logging.Logger;

/**
 *
 * @author Nicola De Maio
 */
@Description("A Concise-type phylogenetic tree.")
public class MultiTypeTreeConcise extends Tree {
	
	public Input<String> countLabelInput = new Input<String>(
            "countLabel",
            "Label for migration counts (default 'count')", "count");

    /*
     * Plugin inputs:
     */
	public Input<Integer> nTypesInput = new Input<Integer>(
            "nTypes",
            "Number of distinct types to consider.", Input.Validate.REQUIRED);
    
    public Input<String> typeLabelInput = new Input<String>(
            "typeLabel",
            "Label for type traits (default 'type')", "type");
    
    /*
     * Non-input fields:
     */
    public String countLabel;
    public String typeLabel;
    protected int nTypes;
    protected TraitSet typeTraitSet;
    protected TraitSet countTraitSet;
    
    protected List <String> typeList;
    protected List <String> countList;

    public MultiTypeTreeConcise() { };
    
    public MultiTypeTreeConcise(MultiTypeNodeConcise rootNode) {
    	//public MultiTypeTreeVolz(Node rootNode) {
        
        if (!(rootNode instanceof MultiTypeNodeConcise)) {
            throw new IllegalArgumentException("Attempted to instantiate "
                    + "multi-type tree with regular root node.");
        }
        
        setRoot(rootNode);
        initArrays();
    }
    
    @Override
    public void initAndValidate() throws IllegalArgumentException {
        
        if (m_initial.get() != null && !(this instanceof StateNodeInitialiser)) {
            
            if (!(m_initial.get() instanceof MultiTypeTreeConcise)) {
                throw new IllegalArgumentException("Attempted to initialise "
                        + "multi-type tree with regular tree object.");
            }
            
            MultiTypeTreeConcise other = (MultiTypeTreeConcise)m_initial.get();
            root = other.root.copy();
            nodeCount = other.nodeCount;
            internalNodeCount = other.internalNodeCount;
            leafNodeCount = other.leafNodeCount;
        }

        if (nodeCount < 0) {
            if (m_taxonset.get() != null) {
                // make a caterpillar
                List<String> sTaxa = m_taxonset.get().asStringList();
                Node left = new MultiTypeNodeConcise();
                left.setNr(0);
                left.setHeight(0);
                left.setID(sTaxa.get(0));
                for (int i = 1; i < sTaxa.size(); i++) {
                    Node right = new MultiTypeNodeConcise();
                    right.setNr(i);
                    right.setHeight(0);
                    right.setID(sTaxa.get(i));
                    Node parent = new MultiTypeNodeConcise();
                    parent.setNr(sTaxa.size() + i - 1);
                    parent.setHeight(i);
                    left.setParent(parent);
                    parent.setLeft(left);
                    right.setParent(parent);
                    parent.setRight(right);
                    left = parent;
                }
                root = left;
                leafNodeCount = sTaxa.size();
                nodeCount = leafNodeCount * 2 - 1;
                internalNodeCount = leafNodeCount - 1;

            } else {
                // make dummy tree with a single root node
                root = new MultiTypeNodeConcise();
                root.setNr(0);
                root.setNr(0);
                root.setTree(this);
                nodeCount = 1;
                internalNodeCount = 0;
                leafNodeCount = 1;
            }
        }

        if (nodeCount >= 0) {
            initArrays();
        }
        
        typeLabel = typeLabelInput.get();
        countLabel = countLabelInput.get();
        nTypes = nTypesInput.get();
        
        processTraits(m_traitList.get());

        // Ensure tree is compatible with traits.
        if (hasDateTrait())
            adjustTreeNodeHeights(root);
    }
    
    @Override
    protected void processTraits(List<TraitSet> traitList) {
        super.processTraits(traitList);
        
        // Record trait set associated with leaf types.
        for (TraitSet traitSet : traitList) {
            if (traitSet.getTraitName().equals(typeLabel)) {
                typeTraitSet = traitSet;
                break;
            }
            if (traitSet.getTraitName().equals(countLabel)) {
                countTraitSet = traitSet;
                break;
            }
        }

        // Construct type list.
        if (typeTraitSet != null) {
            Set<String> typeSet = Sets.newHashSet();
                
            int nTaxa = typeTraitSet.taxaInput.get().asStringList().size();
            for (int i=0; i<nTaxa; i++)
                typeSet.add(typeTraitSet.getStringValue(i));
                
            typeList = Lists.newArrayList(typeSet);
            Collections.sort(typeList);
            
            System.out.println("Type trait with the following types detected:");
            for (int i=0; i<typeList.size(); i++)
                System.out.println(typeList.get(i) + " (" + i + ")");
        }
     // Construct count list.
//        if (countTraitSet != null) {
//            Set<String> countSet = Sets.newHashSet();
//                
//            int nTaxa = countTraitSet.taxaInput.get().asStringList().size();
//            for (int i=0; i<nTaxa; i++)
//                countSet.add(countTraitSet.getStringValue(i));
//                
//            countList = Lists.newArrayList(countSet);
//            Collections.sort(countList);
//            
//            System.out.println("Count trait with the following types detected:");
//            for (int i=0; i<countList.size(); i++)
//                System.out.println(countList.get(i) + " (" + i + ")");
//        }
    }
    
    
    /**
     * @return TraitSet with same name as typeLabel.
     */
    public TraitSet getTypeTrait() {
        if (!traitsProcessed)
            processTraits(m_traitList.get());
        
        return typeTraitSet;
    }
    
    /**
     * @return TraitSet with same name as typeLabel.
     */
    public TraitSet getCountTrait() {
        if (!traitsProcessed)
            processTraits(m_traitList.get());
        
        return countTraitSet;
    }
    
    /**
     * @return true if TraitSet with same name as typeLabel exists.
     */
    public boolean hasTypeTrait() {
        if (getTypeTrait() != null)
            return true;
        else
            return false;
    }
    
    /**
     * @return true if TraitSet with same name as typeLabel exists.
     */
    public boolean hasCountTrait() {
        if (getCountTrait() != null)
            return true;
        else
            return false;
    }
    
    /**
     * Retrieve the list of unique types identified by the type trait.
     * @return List of unique type trait value strings.
     */
    public List<String> getTypeList() {
        if (!traitsProcessed)
            processTraits(m_traitList.get());
        
        return typeList;
    }
    
    /**
     * Retrieve the list of unique types identified by the type trait.
     * @return List of unique type trait value strings.
     */
    public List<String> getCountList() {
        if (!traitsProcessed)
            processTraits(m_traitList.get());
        
        return countList;
    }

    public final void initArrays() {
        // initialise tree-as-array representation + its stored variant
        m_nodes = new MultiTypeNodeConcise[nodeCount];
        listNodes((MultiTypeNodeConcise)root, (MultiTypeNodeConcise[])m_nodes);
        m_storedNodes = new MultiTypeNodeConcise[nodeCount];
        Node copy = root.copy();
        listNodes((MultiTypeNodeConcise)copy, (MultiTypeNodeConcise[])m_storedNodes);
    }
    
    /**
     * Convert Volz-type tree to array representation.
     *
     * @param node Root of sub-tree to convert.
     * @param nodes Array to populate with tree nodes.
     */
    private void listNodes(MultiTypeNodeConcise node, MultiTypeNodeConcise[] nodes) {
        nodes[node.getNr()] = node;
        node.setTree(this);
        if (!node.isLeaf()) {
            listNodes((MultiTypeNodeConcise) node.getLeft(), nodes);
            if (node.getRight()!=null)
                listNodes((MultiTypeNodeConcise) node.getRight(), nodes);
        }
    }
    
    /**
     * Deep copy, returns a completely new Volz-type tree.
     *
     * @return a deep copy of this Volz-type tree
     */
    @Override
    public MultiTypeTreeConcise copy() {
        MultiTypeTreeConcise tree = new MultiTypeTreeConcise();
        tree.ID = ID;
        tree.index = index;
        tree.root = root.copy();
        tree.nodeCount = nodeCount;
        tree.internalNodeCount = internalNodeCount;
        tree.leafNodeCount = leafNodeCount;
        tree.nTypes = nTypes;
        tree.typeLabel = typeLabel;
        tree.countLabel = countLabel;
        return tree;
    }
    
    /**
     * Copy all values from an existing Volz-type tree.
     *
     * @param other
     */
    @Override
    public void assignFrom(StateNode other) {
        MultiTypeTreeConcise mtTree = (MultiTypeTreeConcise) other;

        MultiTypeNodeConcise[] mtNodes = new MultiTypeNodeConcise[mtTree.getNodeCount()];
        for (int i=0; i<mtTree.getNodeCount(); i++)
            mtNodes[i] = new MultiTypeNodeConcise();

        ID = mtTree.ID;
        root = mtNodes[mtTree.root.getNr()];
        root.assignFrom(mtNodes, mtTree.root);
        root.setParent(null);

        nodeCount = mtTree.nodeCount;
        internalNodeCount = mtTree.internalNodeCount;
        leafNodeCount = mtTree.leafNodeCount;
        initArrays();
    }
    
    
    /**
     * Copy all values aside from IDs from an existing Volz-type tree.
     * 
     * @param other
     */
    @Override
    public void assignFromFragile(StateNode other) {
        MultiTypeTreeConcise mtTree = (MultiTypeTreeConcise) other;
        if (m_nodes == null) {
            initArrays();
        }
        root = m_nodes[mtTree.root.getNr()];
        //MultiTypeNodeConcise[] otherNodes = (MultiTypeNodeConcise[]) mtTree.m_nodes;
        Node[] otherNodes = mtTree.m_nodes;
        int iRoot = root.getNr();
        assignFromFragileHelper(0, iRoot, otherNodes);
        root.setHeight(otherNodes[iRoot].getHeight());
        root.setParent(null);
        
        MultiTypeNodeConcise mtRoot = (MultiTypeNodeConcise)root;
        mtRoot.nodeType = ((MultiTypeNodeConcise)(otherNodes[iRoot])).nodeType;
        //mtRoot.migCount = ((MultiTypeNodeConcise)(otherNodes[iRoot])).migCount;
        mtRoot.cacheIndex = ((MultiTypeNodeConcise)(otherNodes[iRoot])).cacheIndex;
        
        if (otherNodes[iRoot].getLeft() != null) {
            root.setLeft(m_nodes[otherNodes[iRoot].getLeft().getNr()]);
        } else {
            root.setLeft(null);
        }
        if (otherNodes[iRoot].getRight() != null) {
            root.setRight(m_nodes[otherNodes[iRoot].getRight().getNr()]);
        } else {
            root.setRight(null);
        }
        assignFromFragileHelper(iRoot + 1, nodeCount, otherNodes);
    }
    
    
    /**
     * helper to assignFromFragile *
     */
    private void assignFromFragileHelper(int iStart, int iEnd, Node[] otherNodes) {
        for (int i = iStart; i < iEnd; i++) {
            MultiTypeNodeConcise sink = (MultiTypeNodeConcise)m_nodes[i];
            MultiTypeNodeConcise src = (MultiTypeNodeConcise)otherNodes[i];
            sink.setHeight(src.getHeight());
            sink.setParent(m_nodes[src.getParent().getNr()]);
            
            sink.cacheIndex = src.cacheIndex;
            sink.nodeType = src.nodeType;
            //sink.migCount = src.migCount;
            
            if (src.getLeft() != null) {
                sink.setLeft(m_nodes[src.getLeft().getNr()]);
                if (src.getRight() != null) {
                    sink.setRight(m_nodes[src.getRight().getNr()]);
                } else {
                    sink.setRight(null);
                }
            }
        }
    }
    
    /**
     * Retrieve total number of allowed types on tree.
     *
     * @return total type/deme count.
     */
    public int getNTypes() {
        return nTypes;
    }
    
    /**
     * Check whether typing and timing of tree are sensible.
     * 
     * @return true if types and times are "valid"
     */
    public boolean isValid() {
        return timesAreValid( root);
    }
 
    private boolean timesAreValid(Node node) {
        for (Node child : node.getChildren()) {
            double lastHeight = node.getHeight();
            if (child.getHeight()>lastHeight)
                return false;
            
            if (!timesAreValid(child))
                return false;
        }
        
        return true;
    }
    
    
    /**
     * Return string representation of Volz-type tree.  We use reflection
     * here to determine whether this is being called as part of writing
     * the state file.
     *
     * @return Volz-type tree string in Newick format.
     */
    @Override
    public String toString() {

        // Behaves differently if writing a state file
        StackTraceElement[] ste = Thread.currentThread().getStackTrace();
        if (ste[2].getMethodName().equals("toXML")) {            
            // Use toShortNewick to generate Newick string without taxon labels
            String string = getNormalTree().getRoot().toShortNewick(true);
            
            // Sanitize ampersands if this is destined for a state file.
            return string.replaceAll("&", "&amp;");
        } else{
            return getNormalTree().getRoot().toSortedNewick(new int[1], true);
        }
    }
    


    /////////////////////////////////////////////////
    //           StateNode implementation          //
    /////////////////////////////////////////////////
    @Override
    protected void store() {
        storedRoot = m_storedNodes[root.getNr()];
        int iRoot = root.getNr();

        storeNodes(0, iRoot);
        
        storedRoot.setHeight(m_nodes[iRoot].getHeight());
        storedRoot.setParent(null);

        if (root.getLeft()!=null)
            storedRoot.setLeft(m_storedNodes[root.getLeft().getNr()]);
        else
            storedRoot.setLeft(null);
        if (root.getRight()!=null)
            storedRoot.setRight(m_storedNodes[root.getRight().getNr()]);
        else
            storedRoot.setRight(null);
        
        MultiTypeNodeConcise mtStoredRoot = (MultiTypeNodeConcise)storedRoot;
        mtStoredRoot.cacheIndex = ((MultiTypeNodeConcise)m_nodes[iRoot]).cacheIndex;
        mtStoredRoot.nodeType = ((MultiTypeNodeConcise)m_nodes[iRoot]).nodeType;
        //mtStoredRoot.migCount = ((MultiTypeNodeConcise)m_nodes[iRoot]).migCount;
        
        storeNodes(iRoot+1, nodeCount);
    }

    /**
     * helper to store *
     */
    private void storeNodes(int iStart, int iEnd) {
        for (int i = iStart; i<iEnd; i++) {
            MultiTypeNodeConcise sink = (MultiTypeNodeConcise)m_storedNodes[i];
            MultiTypeNodeConcise src = (MultiTypeNodeConcise)m_nodes[i];
            sink.setHeight(src.getHeight());
            sink.setParent(m_storedNodes[src.getParent().getNr()]);
            if (src.getLeft()!=null) {
                sink.setLeft(m_storedNodes[src.getLeft().getNr()]);
                if (src.getRight()!=null)
                    sink.setRight(m_storedNodes[src.getRight().getNr()]);
                else
                    sink.setRight(null);
            }
            
            sink.cacheIndex = src.cacheIndex;
            sink.nodeType = src.nodeType;
            //sink.migCount = src.migCount;
        }
    }
    
    
    /////////////////////////////////////////////////
    // Methods implementing the Loggable interface //
    /////////////////////////////////////////////////
    @Override
    public void init(PrintStream printStream) throws IllegalArgumentException {

        printStream.println("#NEXUS\n");
        printStream.println("Begin taxa;");
        printStream.println("\tDimensions ntax="+getLeafNodeCount()+";");
        printStream.println("\t\tTaxlabels");
        for (int i = 0; i<getLeafNodeCount(); i++)
            printStream.println("\t\t\t"+getNodesAsArray()[i].getID());
        printStream.println("\t\t\t;");
        printStream.println("End;");

        printStream.println("Begin trees;");
        printStream.println("\tTranslate");
        for (int i = 0; i<getLeafNodeCount(); i++) {
            printStream.print("\t\t\t"+(getNodesAsArray()[i].getNr()+1)
                    +" "+getNodesAsArray()[i].getID());
            if (i<getLeafNodeCount()-1)
                printStream.print(",");
            printStream.print("\n");
        }
        printStream.print("\t\t\t;");
    }

    @Override
    public void log(long i, PrintStream printStream) {
        printStream.print("tree STATE_"+i+" = ");
        printStream.print(toString());
        printStream.print(";");


    }

    @Override
    public void close(PrintStream printStream) {
        printStream.println("End;");
    }
    
    
    /////////////////////////////////////////////////
    // Methods for flat trees rewritten            //
    /////////////////////////////////////////////////
    
    /**
     * Generates a new tree in which the colours are indicated as meta-data.
     *
     * Caveat: assumes more than one node exists on tree (i.e. leaf != root)
     *
     * @return Normal tree.
     */
    public Tree getNormalTree() {

        // Create new tree to modify.  Note that copy() doesn't
        // initialise the node array lists, so initArrays() must
        // be called manually.
        Tree flatTree = copy();
        flatTree.initArrays();

        //int nextNodeNr = getNodeCount();
        //Node colourChangeNode;

        for (Node node : getNodesAsArray()) {

            MultiTypeNodeConcise mtNode = (MultiTypeNodeConcise)node;

            int nodeNum = node.getNr();

            if (node.isRoot()) {
            	Node startNode = flatTree.getNode(nodeNum);
            	startNode.setMetaData(typeLabel, ((MultiTypeNodeConcise)node).getNodeType());
                startNode.metaDataString = String.format("%s=%d", typeLabel, mtNode.getNodeType());
                startNode.metaDataString = String.format("%s=%d", typeLabel, mtNode.getNodeType());
                //flatTree.getNode(nodeNum).setMetaData(typeLabel,
                //        ((MultiTypeNodeVolz)(node.getLeft())).getNodeType());
                continue;
            }

            Node startNode = flatTree.getNode(nodeNum);
            //Changing to provide in output states for internal nodes!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            startNode.setMetaData(typeLabel, ((MultiTypeNodeConcise)node).getNodeType());
            //startNode.setMetaData(countLabel, ((MultiTypeNodeConcise)node).getMigCount());
            startNode.metaDataString = String.format("%s=%d", typeLabel, mtNode.getNodeType());
            //startNode.metaDataString = String.format("%s=%d", typeLabel, mtNode.getNodeType());
            
        }

        return flatTree;
    }
    
    
    /**
     * Initialise colours and tree topology from Tree object in which colours
     * are marked by meta-data tags.
     *
//     * @param flatTree
//     * @param takeNrsFromFlatTree
     * @throws java.lang.Exception 
     */
    public void initFromNormalTree(Tree tree, boolean takeNrsFromTree, TraitSet type, String typeLabel2) throws Exception {
    	//System.out.println("Start initializing from normal tree.");
        // Build new coloured tree:

        List<Node> activeTreeNodes = new ArrayList<Node>();
        List<Node> nextActiveTreeNodes = new ArrayList<Node>();
        List<MultiTypeNodeConcise> activeVolzTreeNodes = new ArrayList<MultiTypeNodeConcise>();
        List<MultiTypeNodeConcise> nextActiveVolzTreeNodes = new ArrayList<MultiTypeNodeConcise>();

        // Populate active node lists with root:
        activeTreeNodes.add(tree.getRoot());
        MultiTypeNodeConcise newRoot = new MultiTypeNodeConcise();
        activeVolzTreeNodes.add(newRoot);
        
        // Initialise counter used to number leaves when takeNrsFromFlatTree
        // is false:
        int nextNr = 0;
        
        //System.out.println("1 initializing from normal tree.");

        while (!activeTreeNodes.isEmpty()) {

            nextActiveTreeNodes.clear();
            nextActiveVolzTreeNodes.clear();

            for (int idx = 0; idx<activeTreeNodes.size(); idx++) {
                Node treeNode = activeTreeNodes.get(idx);
                MultiTypeNodeConcise volzTreeNode = activeVolzTreeNodes.get(idx);

                List<Integer> colours = new ArrayList<Integer>();
                List<Double> times = new ArrayList<Double>();
                
                //System.out.println("2 initializing from normal tree.");
                while (treeNode.getChildCount()==1) {
                    //int col = (int) Math.round(   (Double) treeNode.getMetaData(typeLabel));
                    //colours.add(col);
                	colours.add(0);
                    times.add(treeNode.getHeight());

                    treeNode = treeNode.getLeft();
                }
                //System.out.println("3 initializing from normal tree.");

                // Order changes from youngest to oldest:
                colours = Lists.reverse(colours);
                times = Lists.reverse(times);
                
                //System.out.println("4 initializing from normal tree.");

                switch (treeNode.getChildCount()) {
                    case 0:
                        // Leaf at base of branch
                        if (takeNrsFromTree) {
                            volzTreeNode.setNr(treeNode.getNr());
                            volzTreeNode.setID(String.valueOf(treeNode.getID()));
                        } else {
                            volzTreeNode.setNr(nextNr);
                            volzTreeNode.setID(String.valueOf(nextNr));
                            nextNr += 1;
                        }
                        
                        //int nodeType = (int) Math.round( (Double) treeNode.getMetaData(typeLabel));
                        //int nodeType = (int) Math.round( (Double) type.getValue(treeNode.getID()));
                        int nodeType =  getTypeList().indexOf(typeTraitSet.getStringValue(treeNode.getID()));
                        volzTreeNode.setNodeType(nodeType);
                        System.out.println("leaf "+treeNode.getID()+" given type "+nodeType);
                        
                        System.out.println("type "+type.getID()+"  "+getTypeList().indexOf(typeTraitSet.getStringValue(treeNode.getID()))+" "+ type.getStringValue(treeNode.getID())+" ");
                        //for (int i = 0; i<typeTraitSet.taxaInput.get().asStringList().size(); i++) {
                        //    leafTypes.add(getTypeList().indexOf(typeTraitSet.getStringValue(i)));
                        //    leafNames.add(typeTraitSet.taxaInput.get().asStringList().get(i));
                        //}
                        
                        break;

                    case 2:
                        // Non-leaf at base of branch
                        nextActiveTreeNodes.add(treeNode.getLeft());
                        nextActiveTreeNodes.add(treeNode.getRight());

                        MultiTypeNodeConcise daughter = new MultiTypeNodeConcise();
                        MultiTypeNodeConcise son = new MultiTypeNodeConcise();
                        volzTreeNode.addChild(daughter);
                        volzTreeNode.addChild(son);
                        nextActiveVolzTreeNodes.add(daughter);
                        nextActiveVolzTreeNodes.add(son);
                        
                        volzTreeNode.setNodeType(0);

                        break;
                }
                
                //System.out.println("5 initializing from normal tree."+treeNode.getMetaData(typeLabel));

                // Set node type at base of multi-type tree branch:
                //int nodeType = (int) Math.round( (Double) treeNode.getMetaData(typeLabel));
                //System.out.println("5.1 initializing from normal tree. "+nodeType);
                //volzTreeNode.setNodeType(nodeType);
                //System.out.println("5.2 initializing from normal tree.");
                
                //int nodeCount = (int) Math.round(
                //        (Double) treeNode.getMetaData(countLabel));
                //volzTreeNode.setMigCount(nodeCount);

                // Set node height:
                volzTreeNode.setHeight(treeNode.getHeight());
                
                //System.out.println("6 initializing from normal tree.");
            }
            
            //System.out.println("7 initializing from normal tree.");

            // Replace old active node lists with new:
            activeTreeNodes.clear();
            activeTreeNodes.addAll(nextActiveTreeNodes);

            activeVolzTreeNodes.clear();
            activeVolzTreeNodes.addAll(nextActiveVolzTreeNodes);
            
            //System.out.println("8 initializing from normal tree.");

        }
        
        //System.out.println("10 initializing from normal tree.");
        
        
        // Number internal nodes:
        numberInternalNodes(newRoot, newRoot.getAllLeafNodes().size());
        
        // Assign tree topology:
        assignFromWithoutID(new MultiTypeTreeConcise(newRoot));
        initArrays();
        //System.out.println("End initializing from normal tree.");
    }
    
    /**
     * Helper method to assign sensible node numbers
     * to each internal node.  This is a post-order traversal, meaning the
     * root is given the largest number.
     * 
     * @param node
     * @param nextNr
     * @return 
     */
    protected int numberInternalNodes(Node node, int nextNr) {
        if (node.isLeaf())
            return nextNr;
        
        for (Node child : node.getChildren())
            nextNr = numberInternalNodes(child, nextNr);
        
        node.setNr(nextNr);
        node.setID(String.valueOf(nextNr));
        
        return nextNr+1;
    }
    
    /////////////////////////////////////////////////
    // Serialization and deserialization for state //
    /////////////////////////////////////////////////
    
//    /**
//     * reconstruct tree from XML fragment in the form of a DOM node *
//     * @param node
//     */
//    @Override
//    public void fromXML(org.w3c.dom.Node node) {
//        try {
//            String sNewick = node.getTextContent().replace("&", "");
//
//            TreeParser parser = new TreeParser();
//            parser.initByName(
//                    "IsLabelledNewick", false,
//                    "offset", 0,
//                    "adjustTipHeights", false,
//                    "singlechild", true,
//                    "newick", sNewick);
//            //parser.m_nThreshold.setValue(1e-10, parser);
//            //parser.m_nOffset.setValue(0, parser);
//            
//            initFromNormalTree(parser, true);
//
//            initArrays();
//        } catch (Exception ex) {
//            Logger.getLogger(MultiTypeTreeConcise.class.getName()).log(Level.SEVERE, null, ex);
//        }
//    }
    
    
}
