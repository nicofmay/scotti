package scotti.evolution.tree;

import beast.base.core.Description;
import beast.base.evolution.tree.Node;
import beast.base.inference.StateNode;
import beast.base.inference.StateNodeInitialiser;
import beast.base.util.Randomizer;

import java.util.List;

/**
 * @author Nicola De Maio
 */
@Description("Class to initialize a MultiTypeTreeVolz from random tree")
public class RandomMultiTypeTreeVolz extends MultiTypeTreeVolz implements StateNodeInitialiser {

    @Override
    public void initAndValidate() throws IllegalArgumentException {
        super.initAndValidate();
        
        if (!hasTypeTrait())
            throw new IllegalArgumentException("No trait set with name '" + typeLabel + "' "
                    + "identified.  Needed to specify taxon locations.");
        
        // Fill leaf colour array:
        for (int i = 0; i<getLeafNodeCount(); i++)
            ((MultiTypeNodeVolz)getNode(i)).setNodeType(
                    getTypeList().indexOf(typeTraitSet.getStringValue(i)));
        
        generateTyping(getRoot());
        
        if (!isValid())
            throw new IllegalArgumentException("Inconsistent colour assignment.");
    }
    
    @Override
    public void initStateNodes() { }
    
    /**
     * Minimal random colouring of tree consistent with leaf colours.
     * 
     * @param node 
     */
    public void generateTyping(Node node){

        if (node.getNodeCount() >= 3){
            Node left = node.getChild(0);
            Node right = node.getChild(1);

            if (left.getNodeCount() >= 3)  generateTyping(left);
            if (right.getNodeCount() >= 3)  generateTyping(right);
            
            int leftCol = ((MultiTypeNodeVolz)left).getNodeType();
            int rightCol = ((MultiTypeNodeVolz)right).getNodeType();

            if (leftCol == rightCol)
                ((MultiTypeNodeVolz)node).setNodeType(leftCol);

            else {

                Node nodeToKeep;//, other;
                if (Randomizer.nextBoolean()) {
                    nodeToKeep = left;
                    //other = right;
                } else {
                    nodeToKeep = right;
                    //other = left;
                }

                ((MultiTypeNodeVolz)node).setNodeType(((MultiTypeNodeVolz)nodeToKeep).getNodeType());

                //double changeTime = Randomizer.nextDouble()*(node.getHeight()-other.getHeight()) + other.getHeight();
                //((MultiTypeNode)other).addChange(((MultiTypeNode)node).getNodeType(), changeTime);
            }
        }
    }

    @Override
    public void getInitialisedStateNodes(List<StateNode> stateNodeList) {
        stateNodeList.add(this);
    }
}
