package scotti.evolution.tree;


import beast.base.core.Description;
import beast.base.core.Input;
import beast.base.core.Input.Validate;
import beast.base.evolution.tree.Tree;
import beast.base.evolution.tree.TreeParser;
import beast.base.inference.StateNode;
import beast.base.inference.StateNodeInitialiser;

import java.util.List;

/**
 * @author Nicola De Maio
 */

@Description("Class to initialize a VolzTree from single child newick tree with type metadata")
public class MultiTypeTreeFromNewickConcise extends MultiTypeTreeConcise implements StateNodeInitialiser {

    public Input<String> newickStringInput = new Input<String>("newick",
            "Tree in Newick format.", Validate.REQUIRED);

    public Input<Boolean> adjustTipHeightsInput = new Input<Boolean>("adjustTipHeights",
            "Adjust tip heights in tree? Default true.", true);

    @Override
    public void initAndValidate() throws IllegalArgumentException {
        
        super.initAndValidate();
        
        TreeParser parser = new TreeParser();
        parser.initByName(
                "IsLabelledNewick", true,
                "adjustTipHeights", adjustTipHeightsInput.get(),
                "singlechild", true,
                "newick", newickStringInput.get());
        Tree tree = parser;
        
        try {
			initFromNormalTree(tree, true, this.typeTraitSet, typeLabel);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

    @Override
    public void initStateNodes() { }

    @Override
    public void getInitialisedStateNodes(List<StateNode> stateNodeList) {
        stateNodeList.add(this);
    }
}
