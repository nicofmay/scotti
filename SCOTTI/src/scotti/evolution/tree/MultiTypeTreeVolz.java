/*
 * Copyright (C) 2014 Nicola De Maio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package scotti.evolution.tree;

import beast.base.core.Description;
import beast.base.core.Input;
import beast.base.evolution.tree.Node;
import beast.base.evolution.tree.TraitSet;
import beast.base.evolution.tree.Tree;
import beast.base.evolution.tree.TreeParser;
import beast.base.inference.StateNode;
import beast.base.inference.StateNodeInitialiser;
import beastfx.app.inputeditor.BeautiDoc;
import com.google.common.collect.Lists;

import java.io.PrintStream;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Nicola De Maio
 */
@Description("A Volz-type phylogenetic tree not using NTypes.")
public class MultiTypeTreeVolz extends Tree {

    /*
     * Plugin inputs:
     */
//    public Input<Integer> nTypesInput = new Input<Integer>(
//            "nTypes",
//            "Number of distinct types to consider.", Input.Validate.REQUIRED);
    
    public Input<String> typeLabelInput = new Input<String>(
            "typeLabel",
            "Label for type traits (default 'type')", "type");
    
    public Input<TraitSet> typeTraitInput = new Input<>(
            "typeTrait", "Type trait set.  Used only by BEAUti.");

    public Input<List<String>> typeTraitValuesInput = new Input<>(
            "typeTraitValue",
            "An additional type value to be included even when absent " +
            "from the sampled taxa.", new ArrayList<>());
    
    /*
     * Non-input fields:
     */
    protected String typeLabel;
    //protected int nTypes;
    protected TraitSet typeTraitSet;
    
    protected List <String> typeList;
   

    public MultiTypeTreeVolz() { };
    
    public MultiTypeTreeVolz(MultiTypeNodeVolz rootNode) {
    	//public MultiTypeTreeVolz(Node rootNode) {
        
        if (!(rootNode instanceof MultiTypeNodeVolz)) {
            throw new IllegalArgumentException("Attempted to instantiate "
                    + "multi-type tree with regular root node.");
        }
        
        setRoot(rootNode);
        initArrays();
    }
    
    @Override
    public void initAndValidate() throws IllegalArgumentException {
        
        if (m_initial.get() != null && !(this instanceof StateNodeInitialiser)) {
            
            if (!(m_initial.get() instanceof MultiTypeTreeVolz)) {
                throw new IllegalArgumentException("Attempted to initialise "
                        + "multi-type tree with regular tree object.");
            }
            
            MultiTypeTreeVolz other = (MultiTypeTreeVolz)m_initial.get();
            root = other.root.copy();
            nodeCount = other.nodeCount;
            internalNodeCount = other.internalNodeCount;
            leafNodeCount = other.leafNodeCount;
        }

        if (nodeCount < 0) {
            if (m_taxonset.get() != null) {
                // make a caterpillar
                List<String> sTaxa = m_taxonset.get().asStringList();
                Node left = new MultiTypeNodeVolz();
                left.setNr(0);
                left.setHeight(0);
                left.setID(sTaxa.get(0));
                for (int i = 1; i < sTaxa.size(); i++) {
                    Node right = new MultiTypeNodeVolz();
                    right.setNr(i);
                    right.setHeight(0);
                    right.setID(sTaxa.get(i));
                    Node parent = new MultiTypeNodeVolz();
                    parent.setNr(sTaxa.size() + i - 1);
                    parent.setHeight(i);
                    left.setParent(parent);
                    parent.setLeft(left);
                    right.setParent(parent);
                    parent.setRight(right);
                    left = parent;
                }
                root = left;
                leafNodeCount = sTaxa.size();
                nodeCount = leafNodeCount * 2 - 1;
                internalNodeCount = leafNodeCount - 1;

            } else {
                // make dummy tree with a single root node
                root = new MultiTypeNodeVolz();
                root.setNr(0);
                root.setNr(0);//TODO why twice?
                root.setTree(this);
                nodeCount = 1;
                internalNodeCount = 0;
                leafNodeCount = 1;
            }
        }

        if (nodeCount >= 0) {
            initArrays();
        }
        
        typeLabel = typeLabelInput.get();
        
        //nTypes = nTypesInput.get();
        
        processTraits(m_traitList.get());

        // Ensure tree is compatible with traits.
        if (hasDateTrait())
            adjustTreeNodeHeights(root);
    }
    
    
    
    
    
    @Override
    protected void processTraits(List<TraitSet> traitList) {
        super.processTraits(traitList);
        
        // Record trait set associated with leaf types.
        for (TraitSet traitSet : traitList) {
            if (traitSet.getTraitName().equals(typeLabel)) {
                typeTraitSet = traitSet;
                break;
            }
        }

        // Use explicitly-identified type trait set if available.
        // Seems dumb, but needed for BEAUti as ListInputEditors
        // muck things up...
        if (typeTraitInput.get() != null)
            typeTraitSet = typeTraitInput.get();

        // Construct type list.
        if (typeTraitSet == null) {
            if (getTaxonset() != null) {
                TraitSet dummyTraitSet = new TraitSet();

                StringBuilder sb = new StringBuilder();
                for (int i=0; i<getTaxonset().getTaxonCount(); i++) {
                    if (i>0)
                        sb.append(",\n");
                    sb.append(getTaxonset().getTaxonId(i)).append("=NOT_SET");
                }
                try {
                    dummyTraitSet.initByName(
                        "traitname", "type",
                        "taxa", getTaxonset(),
                        "value", sb.toString());
                    //TODO this may move to beast.base
                    dummyTraitSet.setID("typeTraitSet.t:"
                        + BeautiDoc.parsePartition(getID()));
                    setTypeTrait(dummyTraitSet);
                } catch (Exception ex) {
                    System.out.println("Error setting default type trait.");
                }
            }
        }

        if (typeTraitSet != null) {

            Set<String> typeSet = new HashSet<>();

            int nTaxa = typeTraitSet.taxaInput.get().asStringList().size();
            for (int i = 0; i < nTaxa; i++)
                typeSet.add(typeTraitSet.getStringValue(i));

            // Include any addittional trait values in type list
            for (String typeName : typeTraitValuesInput.get())
                typeSet.add(typeName);

            typeList = Lists.newArrayList(typeSet);
            Collections.sort(typeList);

            System.out.println("Type trait with the following types detected:");
            for (int i = 0; i < typeList.size(); i++)
                System.out.println(typeList.get(i) + " (" + i + ")");

        }
    }
    
    
    
    
    
    
    
    
    /**
     * @return TraitSet with same name as typeLabel.
     */
    public TraitSet getTypeTrait() {
        if (!traitsProcessed)
            processTraits(m_traitList.get());
        
        return typeTraitSet;
    }
    
    /**
     * @return true if TraitSet with same name as typeLabel exists.
     */
    public boolean hasTypeTrait() {
        if (getTypeTrait() != null)
            return true;
        else
            return false;
    }
    
    /**
     * Specifically set the type trait set for this tree. A null value simply
     * removes the existing trait set.
     *
     * @param traitSet
     */
    public void setTypeTrait(TraitSet traitSet) {
        if (hasTypeTrait()) {
            m_traitList.get().remove(typeTraitSet);
        }

        if (traitSet != null) {
            //m_traitList.setValue(traitSet, this);
            typeTraitInput.setValue(traitSet, this);
        }

        typeTraitSet = traitSet;
    }
    
    /**
     * Retrieve the list of unique types identified by the type trait.
     * @return List of unique type trait value strings.
     */
    public List<String> getTypeList() {
        if (!traitsProcessed)
            processTraits(m_traitList.get());
        
        return typeList;
    }
    
    
    /**
     * @param type
     * @return string name of given type
     */
    public String getTypeString(int type) {
        if (!traitsProcessed)
            processTraits(m_traitList.get());

        return typeList.get(type);
    }

    /**
     * @param typeString
     * @return integer type corresponding to given type string
     */
    public int getTypeFromString(String typeString) {
        if (!traitsProcessed)
            processTraits(m_traitList.get());

        return typeList.indexOf(typeString);
    }

    /**
     * @return type label to be used in logging.
     */
    public String getTypeLabel() {
        return typeLabel;
    }
    
    
    
    @Override
    public void initArrays() {
        // initialise tree-as-array representation + its stored variant
        m_nodes = new MultiTypeNodeVolz[nodeCount];
        listNodes((MultiTypeNodeVolz)root, (MultiTypeNodeVolz[])m_nodes);
        m_storedNodes = new MultiTypeNodeVolz[nodeCount];
        Node copy = root.copy();
        listNodes((MultiTypeNodeVolz)copy, (MultiTypeNodeVolz[])m_storedNodes);
    }

    /**
     * Convert Volz-type tree to array representation.
     *
     * @param node Root of sub-tree to convert.
     * @param nodes Array to populate with tree nodes.
     */
    private void listNodes(MultiTypeNodeVolz node, MultiTypeNodeVolz[] nodes) {
        nodes[node.getNr()] = node;
        node.setTree(this);
        if (!node.isLeaf()) {
            listNodes((MultiTypeNodeVolz) node.getLeft(), nodes);
            if (node.getRight()!=null)
                listNodes((MultiTypeNodeVolz) node.getRight(), nodes);
        }
    }

//    /**
//     * Deep copy, returns a completely new Volz-type tree.
//     *
//     * @return a deep copy of this Volz-type tree
//     */
//    @Override
//    public MultiTypeTreeVolz copy() {
//        MultiTypeTreeVolz tree = new MultiTypeTreeVolz();
//        tree.ID = ID;
//        tree.index = index;
//        tree.root = root.copy();
//        tree.nodeCount = nodeCount;
//        tree.internalNodeCount = internalNodeCount;
//        tree.leafNodeCount = leafNodeCount;
//        tree.nTypes = nTypes;
//        tree.typeLabel = typeLabel;
//        return tree;
//    }
    
    /**
     * Deep copy, returns a completely new multi-type tree.
     *
     * @return a deep copy of this multi-type tree
     */
    @Override
    public MultiTypeTreeVolz copy() {
        MultiTypeTreeVolz tree = new MultiTypeTreeVolz();
        tree.ID = ID;
        tree.index = index;
        tree.root = root.copy();
        tree.nodeCount = nodeCount;
        tree.internalNodeCount = internalNodeCount;
        tree.leafNodeCount = leafNodeCount;
        tree.typeLabel = typeLabel;
        return tree;
    }

    /**
     * Copy all values from an existing Volz-type tree.
     *
     * @param other
     */
    @Override
    public void assignFrom(StateNode other) {
        MultiTypeTreeVolz mtTree = (MultiTypeTreeVolz) other;

        MultiTypeNodeVolz[] mtNodes = new MultiTypeNodeVolz[mtTree.getNodeCount()];
        for (int i=0; i<mtTree.getNodeCount(); i++)
            mtNodes[i] = new MultiTypeNodeVolz();

        ID = mtTree.ID;
        root = mtNodes[mtTree.root.getNr()];
        root.assignFrom(mtNodes, mtTree.root);
        root.setParent(null);

        nodeCount = mtTree.nodeCount;
        internalNodeCount = mtTree.internalNodeCount;
        leafNodeCount = mtTree.leafNodeCount;
        initArrays();
    }
    
    /**
     * Copy all values aside from IDs from an existing Volz-type tree.
     * 
     * @param other
     */
    @Override
    public void assignFromFragile(StateNode other) {
        MultiTypeTreeVolz mtTree = (MultiTypeTreeVolz) other;
        if (m_nodes == null) {
            initArrays();
        }
        root = m_nodes[mtTree.root.getNr()];
        //MultiTypeNodeVolz[] otherNodes = (MultiTypeNodeVolz[]) mtTree.m_nodes;
        Node[] otherNodes = mtTree.m_nodes;
        int iRoot = root.getNr();
        assignFromFragileHelper(0, iRoot, otherNodes);
        root.setHeight(otherNodes[iRoot].getHeight());
        root.setParent(null);
        
        MultiTypeNodeVolz mtRoot = (MultiTypeNodeVolz)root;
        mtRoot.nodeType = ((MultiTypeNodeVolz)(otherNodes[iRoot])).nodeType;
        mtRoot.cacheIndex = ((MultiTypeNodeVolz)(otherNodes[iRoot])).cacheIndex;
        
        if (otherNodes[iRoot].getLeft() != null) {
            root.setLeft(m_nodes[otherNodes[iRoot].getLeft().getNr()]);
        } else {
            root.setLeft(null);
        }
        if (otherNodes[iRoot].getRight() != null) {
            root.setRight(m_nodes[otherNodes[iRoot].getRight().getNr()]);
        } else {
            root.setRight(null);
        }
        assignFromFragileHelper(iRoot + 1, nodeCount, otherNodes);
    }

    /**
     * helper to assignFromFragile *
     */
    private void assignFromFragileHelper(int iStart, int iEnd, Node[] otherNodes) {
        for (int i = iStart; i < iEnd; i++) {
            MultiTypeNodeVolz sink = (MultiTypeNodeVolz)m_nodes[i];
            MultiTypeNodeVolz src = (MultiTypeNodeVolz)otherNodes[i];
            sink.setHeight(src.getHeight());
            sink.setParent(m_nodes[src.getParent().getNr()]);
            
            sink.cacheIndex = src.cacheIndex;
            sink.nodeType = src.nodeType;
            
            if (src.getLeft() != null) {
                sink.setLeft(m_nodes[src.getLeft().getNr()]);
                if (src.getRight() != null) {
                    sink.setRight(m_nodes[src.getRight().getNr()]);
                } else {
                    sink.setRight(null);
                }
            }
        }
    }

//    /**
//     * Retrieve total number of allowed types on tree.
//     *
//     * @return total type/deme count.
//     */
//    public int getNTypes() {
//        return nTypes;
//    }
    
    /**
     * Obtain the number of types defined for this MultiTypeTreeVolz.
     *
     * @return number of types defined for MultiTypeTreeVolz
     */
    public int getNTypes() {
        if (!traitsProcessed)
            processTraits(m_traitList.get());

        return typeList.size();
    }
    
    /**
     * Check whether typing and timing of tree are sensible.
     * 
     * @return true if types and times are "valid"
     */
    public boolean isValid() {
        return timesAreValid( root);
    }
 
    private boolean timesAreValid(Node node) {
        for (Node child : node.getChildren()) {
            double lastHeight = node.getHeight();
            if (child.getHeight()>lastHeight)
                return false;
            
            if (!timesAreValid(child))
                return false;
        }
        
        return true;
    }
    

    /**
     * Return string representation of Volz-type tree.  We use reflection
     * here to determine whether this is being called as part of writing
     * the state file.
     *
     * @return Volz-type tree string in Newick format.
     */
    @Override
    public String toString() {

        // Behaves differently if writing a state file
        StackTraceElement[] ste = Thread.currentThread().getStackTrace();
        if (ste[2].getMethodName().equals("toXML")) {            
            // Use toShortNewick to generate Newick string without taxon labels
            String string = getNormalTree().getRoot().toShortNewick(true);
            
            // Sanitize ampersands if this is destined for a state file.
            return string.replaceAll("&", "&amp;");
        } else{
            return getNormalTree().getRoot().toSortedNewick(new int[1], true);
        }
    }


    /////////////////////////////////////////////////
    //           StateNode implementation          //
    /////////////////////////////////////////////////
    @Override
    protected void store() {
        storedRoot = m_storedNodes[root.getNr()];
        int iRoot = root.getNr();

        storeNodes(0, iRoot);
        
        storedRoot.setHeight(m_nodes[iRoot].getHeight());
        storedRoot.setParent(null);

        if (root.getLeft()!=null)
            storedRoot.setLeft(m_storedNodes[root.getLeft().getNr()]);
        else
            storedRoot.setLeft(null);
        if (root.getRight()!=null)
            storedRoot.setRight(m_storedNodes[root.getRight().getNr()]);
        else
            storedRoot.setRight(null);
        
        MultiTypeNodeVolz mtStoredRoot = (MultiTypeNodeVolz)storedRoot;
        mtStoredRoot.cacheIndex = ((MultiTypeNodeVolz)m_nodes[iRoot]).cacheIndex;
        mtStoredRoot.nodeType = ((MultiTypeNodeVolz)m_nodes[iRoot]).nodeType;
        
        storeNodes(iRoot+1, nodeCount);
    }

    /**
     * helper to store *
     */
    private void storeNodes(int iStart, int iEnd) {
        for (int i = iStart; i<iEnd; i++) {
            MultiTypeNodeVolz sink = (MultiTypeNodeVolz)m_storedNodes[i];
            MultiTypeNodeVolz src = (MultiTypeNodeVolz)m_nodes[i];
            sink.setHeight(src.getHeight());
            sink.setParent(m_storedNodes[src.getParent().getNr()]);
            if (src.getLeft()!=null) {
                sink.setLeft(m_storedNodes[src.getLeft().getNr()]);
                if (src.getRight()!=null)
                    sink.setRight(m_storedNodes[src.getRight().getNr()]);
                else
                    sink.setRight(null);
            }
            
            sink.cacheIndex = src.cacheIndex;
            sink.nodeType = src.nodeType;
        }
    }

    /////////////////////////////////////////////////
    // Methods implementing the Loggable interface //
    /////////////////////////////////////////////////
    @Override
    public void init(PrintStream printStream) throws IllegalArgumentException {

        printStream.println("#NEXUS\n");
        printStream.println("Begin taxa;");
        printStream.println("\tDimensions ntax="+getLeafNodeCount()+";");
        printStream.println("\t\tTaxlabels");
        for (int i = 0; i<getLeafNodeCount(); i++)
            printStream.println("\t\t\t"+getNodesAsArray()[i].getID());
        printStream.println("\t\t\t;");
        printStream.println("End;");

        printStream.println("Begin trees;");
        printStream.println("\tTranslate");
        for (int i = 0; i<getLeafNodeCount(); i++) {
            printStream.print("\t\t\t"+(getNodesAsArray()[i].getNr()+1)
                    +" "+getNodesAsArray()[i].getID());
            if (i<getLeafNodeCount()-1)
                printStream.print(",");
            printStream.print("\n");
        }
        printStream.print("\t\t\t;");
    }

    @Override
    public void log(long i, PrintStream printStream) {
        printStream.print("tree STATE_"+i+" = ");
        printStream.print(toString());
        printStream.print(";");


    }

    @Override
    public void close(PrintStream printStream) {
        printStream.println("End;");
    }
    
    
    /////////////////////////////////////////////////
    // Methods for flat trees rewritten            //
    /////////////////////////////////////////////////
    
    /**
     * Generates a new tree in which the colours are indicated as meta-data.
     *
     * Caveat: assumes more than one node exists on tree (i.e. leaf != root)
     *
     * @return Normal tree.
     */
    public Tree getNormalTree() {

        // Create new tree to modify.  Note that copy() doesn't
        // initialise the node array lists, so initArrays() must
        // be called manually.
        Tree flatTree = copy();
        flatTree.initArrays();

        //int nextNodeNr = getNodeCount();
        //Node colourChangeNode;

        for (Node node : getNodesAsArray()) {

            MultiTypeNodeVolz mtNode = (MultiTypeNodeVolz)node;

            int nodeNum = node.getNr();

            if (node.isRoot()) {
            	Node startNode = flatTree.getNode(nodeNum);
            	startNode.setMetaData(typeLabel, ((MultiTypeNodeVolz)node).getNodeType());
                startNode.metaDataString = String.format("%s=%d", typeLabel, mtNode.getNodeType());
                //flatTree.getNode(nodeNum).setMetaData(typeLabel,
                //        ((MultiTypeNodeVolz)(node.getLeft())).getNodeType());
                continue;
            }

            Node startNode = flatTree.getNode(nodeNum);
            //Changing to provide in output states for internal nodes!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            startNode.setMetaData(typeLabel, ((MultiTypeNodeVolz)node).getNodeType());
            startNode.metaDataString = String.format("%s=%d", typeLabel, mtNode.getNodeType());
            
//            if (startNode.isLeaf()){
//            	startNode.setMetaData(typeLabel,
//                        ((MultiTypeNodeVolz)node).getNodeType());
//                startNode.metaDataString = String.format("%s=%d",
//                        typeLabel, mtNode.getNodeType());
//            }
            
            
            
            

            //Node endNode = startNode.getParent();
            
            //endNode.setMetaData(typeLabel,
            //        ((MultiTypeNode)node.getParent()).getNodeType());
            //endNode.metaDataString = String.format("%s=%d",
            //        typeLabel, ((MultiTypeNode)node.getParent()).getNodeType());

            // Ensure final branchNode is connected to the original parent:
            //branchNode.setParent(endNode);
            //if (endNode.getLeft()==startNode)
            //    endNode.setLeft(branchNode);
            //else
            //    endNode.setRight(branchNode);
        }

        return flatTree;
    }
    
    
    /**
     * Initialise colours and tree topology from Tree object in which colours
     * are marked by meta-data tags.
     *
//     * @param flatTree
//     * @param takeNrsFromFlatTree
     * @throws java.lang.Exception 
     */
    public void initFromNormalTree(Tree tree, boolean takeNrsFromTree) throws Exception {

        // Build new coloured tree:

        List<Node> activeTreeNodes = new ArrayList<Node>();
        List<Node> nextActiveTreeNodes = new ArrayList<Node>();
        List<MultiTypeNodeVolz> activeVolzTreeNodes = new ArrayList<MultiTypeNodeVolz>();
        List<MultiTypeNodeVolz> nextActiveVolzTreeNodes = new ArrayList<MultiTypeNodeVolz>();

        // Populate active node lists with root:
        activeTreeNodes.add(tree.getRoot());
        MultiTypeNodeVolz newRoot = new MultiTypeNodeVolz();
        activeVolzTreeNodes.add(newRoot);
        
        // Initialise counter used to number leaves when takeNrsFromFlatTree
        // is false:
        int nextNr = 0;

        while (!activeTreeNodes.isEmpty()) {

            nextActiveTreeNodes.clear();
            nextActiveVolzTreeNodes.clear();

            for (int idx = 0; idx<activeTreeNodes.size(); idx++) {
                Node treeNode = activeTreeNodes.get(idx);
                MultiTypeNodeVolz volzTreeNode = activeVolzTreeNodes.get(idx);

                List<Integer> colours = new ArrayList<Integer>();
                List<Double> times = new ArrayList<Double>();

                while (treeNode.getChildCount()==1) {
                    int col = (int) Math.round(
                            (Double) treeNode.getMetaData(typeLabel));
                    colours.add(col);
                    times.add(treeNode.getHeight());

                    treeNode = treeNode.getLeft();
                }

                // Order changes from youngest to oldest:
                colours = Lists.reverse(colours);
                times = Lists.reverse(times);

                switch (treeNode.getChildCount()) {
                    case 0:
                        // Leaf at base of branch
                        if (takeNrsFromTree) {
                            volzTreeNode.setNr(treeNode.getNr());
                            volzTreeNode.setID(String.valueOf(treeNode.getID()));
                        } else {
                            volzTreeNode.setNr(nextNr);
                            volzTreeNode.setID(String.valueOf(nextNr));
                            nextNr += 1;
                        }
                        break;

                    case 2:
                        // Non-leaf at base of branch
                        nextActiveTreeNodes.add(treeNode.getLeft());
                        nextActiveTreeNodes.add(treeNode.getRight());

                        MultiTypeNodeVolz daughter = new MultiTypeNodeVolz();
                        MultiTypeNodeVolz son = new MultiTypeNodeVolz();
                        volzTreeNode.addChild(daughter);
                        volzTreeNode.addChild(son);
                        nextActiveVolzTreeNodes.add(daughter);
                        nextActiveVolzTreeNodes.add(son);

                        break;
                }

                // Set node type at base of multi-type tree branch:
                int nodeType = (int) Math.round(
                        (Double) treeNode.getMetaData(typeLabel));
                volzTreeNode.setNodeType(nodeType);

                // Set node height:
                volzTreeNode.setHeight(treeNode.getHeight());
            }

            // Replace old active node lists with new:
            activeTreeNodes.clear();
            activeTreeNodes.addAll(nextActiveTreeNodes);

            activeVolzTreeNodes.clear();
            activeVolzTreeNodes.addAll(nextActiveVolzTreeNodes);

        }
        
        
        // Number internal nodes:
        numberInternalNodes(newRoot, newRoot.getAllLeafNodes().size());
        
        // Assign tree topology:
        assignFromWithoutID(new MultiTypeTreeVolz(newRoot));
        initArrays();
        
    }
    
    /**
     * Helper method to assign sensible node numbers
     * to each internal node.  This is a post-order traversal, meaning the
     * root is given the largest number.
     * 
     * @param node
     * @param nextNr
     * @return 
     */
    protected int numberInternalNodes(Node node, int nextNr) {
        if (node.isLeaf())
            return nextNr;
        
        for (Node child : node.getChildren())
            nextNr = numberInternalNodes(child, nextNr);
        
        node.setNr(nextNr);
        node.setID(String.valueOf(nextNr));
        
        return nextNr+1;
    }
    
    
    /////////////////////////////////////////////////
    // Serialization and deserialization for state //
    /////////////////////////////////////////////////
    
    /**
     * reconstruct tree from XML fragment in the form of a DOM node *
     * @param node
     */
    @Override
    public void fromXML(org.w3c.dom.Node node) {
        try {
            String sNewick = node.getTextContent().replace("&", "");

            TreeParser parser = new TreeParser();
            parser.initByName(
                    "IsLabelledNewick", false,
                    "offset", 0,
                    "adjustTipHeights", false,
                    "singlechild", true,
                    "newick", sNewick);
            //parser.m_nThreshold.setValue(1e-10, parser);
            //parser.m_nOffset.setValue(0, parser);
            
            initFromNormalTree(parser, true);

            initArrays();
        } catch (Exception ex) {
            Logger.getLogger(MultiTypeTreeVolz.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
