SCOTTI
=============

SCOTTI (Structured COalescent Transmission Tree Inference) is a BEAST 2 package to reconstruct transmission trees from genetic data of the pathogen and epidemiological data simultaneously.
This program has been developed by heavily using the code from MultiTypeTree written by Tim Vaughan and colleagues, and BASTA, written by ourselves.


Creator: Nicola De Maio.

Contributors: Jessie Wu, Daniel Wilson.


The easiest way to install SCOTTI is through BEAUti in BEAST2, by just clicking on "manage packages" and then on "SCOTTI", "INSTALL/UPGRADE". As of latest BEAST2 distribution, this requires Java 8 to be installed. Generating a graphical output with the script "Make_transmission_tree.py" in the SCOTTI folder will also require the graph_tool and numpy libraries in Python to be installed and up to date.


We now also provide a script "SCOTTI_generate_xml.py" that generates an xml file for a SCOTTI analysis. To use this script, run for example:

```python
python2 SCOTTI_generate_xml.py -f SCOTTI_example.fasta -o SCOTTI_example_output -d SCOTTI_example_dates.csv -ho SCOTTI_example_hosts.csv -ht SCOTTI_example_hostTimes.csv -ov -m 6 -n 100000
```

where SCOTTI_example.fasta is a fasta alignment, SCOTTI_example_output is the name to be given to the output, SCOTTI_example_dates.csv contains the dates (converted into numbers) of sampling of samples, SCOTTI_example_hosts.csv contains the hosts from which samples were taken, and SCOTTI_example_hostTimes.csv contains the time information of hosts (an example is provided for all these files in the SCOTTI folder). The maximum number of hosts allowed can be changed with option -m, while the number of MCMC iterations with option -n.

Details and examples on how to write a SCOTTI xml file, which then has to be run in BEAST2, are also provided in the documentation (doc folder in https://bitbucket.org/nicofmay/scotti/src/ -> SCOTTI). 


If you want to run SCOTTI from command line, first download and unzip the gitHub folder, then execute:

java -cp /SCOTTIfolder/dist/SCOTTI.v1.1.0/lib/SCOTTI.v1.1.0.jar:/SCOTTIfolder/lib/jblas-1.2.3.jar:/SCOTTIfolder/lib/guava-15.0.jar:/BEASTfolder/lib/beast.jar beast.app.beastapp.BeastMain SCOTTI_XML_file.xml

Where:
"SCOTTIfolder" stands for the downloaded SCOTTI package path;
"BEASTfolder" stands for the BEAST2 folder path;
"SCOTTI_XML_file.xml" stands for the newly created SCOTTI xml file, including path.

Otherwise it is generally possible to run SCOTTI by selecting the relative SCOTTI xml file after double-clicking on the BEAST2 application.



License
-------

This software is free (as in freedom).  With the exception of the
libraries on which it depends, it is made available under the terms of
the GNU General Public Licence version 3, which is contained in this
directory in the file named COPYING.

The following libraries are bundled with SCOTTI:

* Google Guava (http://code.google.com/p/guava-libraries/)
* jblas (http://mikiobraun.github.io/jblas/)

That software is distributed under the licences provided in the
LICENCE.* files included in this archive.

This project has been supported by the Oxford Martin School.