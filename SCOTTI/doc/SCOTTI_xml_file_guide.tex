

\documentclass[10pt,letterpaper]{article}
\usepackage[top=0.85in,left=2.75in,footskip=0.75in]{geometry}

% Use adjustwidth environment to exceed column width (see example table in text)
\usepackage{changepage}

% Use Unicode characters when possible
\usepackage[utf8]{inputenc}

% textcomp package and marvosym package for additional characters
\usepackage{textcomp,marvosym}

% fixltx2e package for \textsubscript
\usepackage{fixltx2e}

% amsmath and amssymb packages, useful for mathematical formulas and symbols
\usepackage{amsmath,amssymb}

% cite package, to clean up citations in the main text. Do not remove.
\usepackage{cite}

% Use nameref to cite supporting information files (see Supporting Information section for more info)
\usepackage{nameref,hyperref}

% line numbers
\usepackage[right]{lineno}

\usepackage{listings}


% ligatures disabled
\usepackage{microtype}
\DisableLigatures[f]{encoding = *, family = * }

% rotating package for sideways tables
\usepackage{rotating}

%bold math font
\usepackage{bm}

\usepackage{color}

% Remove comment for double spacing
%\usepackage{setspace} 
%\doublespacing

% Text layout
\raggedright
\setlength{\parindent}{0.5cm}
\textwidth 5.25in 
\textheight 8.75in

% Bold the 'Figure #' in the caption and separate it from the title/caption with a period
% Captions will be left justified
\usepackage[aboveskip=1pt,labelfont=bf,labelsep=period,justification=raggedright,singlelinecheck=off]{caption}

% Use the PLoS provided BiBTeX style
%\bibliographystyle{plos2009}

% Remove brackets from numbering in List of References
\makeatletter
\renewcommand{\@biblabel}[1]{\quad#1.}
\makeatother

% Leave date blank
\date{}

%% END MACROS SECTION


\begin{document}
\vspace*{0.35in}

% Title must be 150 characters or less

{\Huge
\textbf\newline{How to write/edit an xml file for SCOTTI}
}
\newline

{\Large
\textbf\newline{Nicola De Maio}
}
\newline
{
\textbf\newline{\today}
}
\newline

\definecolor{dkgreen}{rgb}{0,0.6,0}
\definecolor{gray}{rgb}{0.5,0.5,0.5}
\definecolor{mauve}{rgb}{0.58,0,0.82}
\lstset{frame=tb,
  language=Java,
  aboveskip=3mm,
  belowskip=3mm,
  showstringspaces=false,
  columns=flexible,
  basicstyle={\small\ttfamily},
  numbers=none,
  numberstyle=\tiny\color{gray},
  keywordstyle=\color{blue},
  commentstyle=\color{dkgreen},
  stringstyle=\color{mauve},
  breaklines=true,
  breakatwhitespace=true,
  tabsize=3
}

%{\huge
%\textcolor{green}{Green: things still to do/modify}
%}
%\newline

\section{Installing SCOTTI}

SCOTTI is a BEAST2 package. 
To install it, first download and install BEAST2 if you have not already done so (\emph{http://beast2.org/}).
Then, from the BEAST2 distribution, run BEAUti (double-click on the BEAUti icon contained in the BEAST2 folder).
In BEAUti, click on "File" from the top menu, select "Manage packages" from the scroll-down menu, then select SCOTTI from the table of packages, and click on "Install/Upgrade".
Now BEAST2 will be able of running SCOTTI xml files.

The source code of SCOTTI, examples, and this documentation, can be also found in  \emph{https://bitbucket.org/nicofmay/scotti} which also includes an alternative installation procedure and terminal running instructions in the read me file.

\section{Creating a working xml file}

We now provide a python script called "SCOTTI\_generate\_xml.py" to generate a SCOTTI xml file to run a SCOTTI analysis. To use this script, run for example:

\begin{lstlisting}
python SCOTTI_generate_xml.py -f SCOTTI_example.fasta -o SCOTTI_example_output -d SCOTTI_example_dates.csv -ho SCOTTI_example_hosts.csv -ht SCOTTI_example_hostTimes.csv -ov -m 6 -n 100000
\end{lstlisting}

where "SCOTTI\_example.fasta" is a fasta alignment, "SCOTTI\_example\_output" is the name to be given to the output, "SCOTTI\_example\_dates.csv" contains the dates (converted into numbers) of sampling of samples, "SCOTTI\_example\_hosts.csv" contains the hosts from which samples were taken, and "SCOTTI\_example\_hostTimes.csv" contains the time information of hosts (an example is provided for all of these files in the SCOTTI folder). The maximum number of hosts allowed can be changed with option -m, while the number of MCMC iterations with option -n.

An alternative to using this python script, is using one of the example SCOTTI xml files provided in the "example" folder as template, and manually adding/modifying parts as explained below. 
This it is possible to run an analysis with customised features. 
In the following, I will assume that the "SCOTTI\_KPneu.xml" example file has been chosen as a template.

\subsection{Including the genetic alignment}

A genetic alignment section will look like this:

\begin{lstlisting}
<alignment spec="beast.evolution.alignment.Alignment" id="alignment1" dataType="nucleotide">
<sequence taxon='Taxon1' value="atgtgggatccacttctaaataa"/>
<sequence taxon='Taxon2' value="atgtgggatccacttctaaattaa"/>
</alignment>
\end{lstlisting}

Here "alignment1" is the name of the alignment (important to distinguish eventual different alignments), and "Taxon1" is the name of the first taxon, etc.
You can manually edit this part to enter your own alignment, and you can even enter multiple alignments, for example if you have different loci for which you want to estimate different phylogenetic trees, but which share the mutation or migration rates. For a large alignment, the simplest way to create this section is to use BEAUti: open the program, click on the "+" button on the bottom, and choose your alignment. Then save the xml file ("File"$\rightarrow$"Save as") and from the saved file select the alignment section (and only that) and past it in your SCOTTI xml file.

\subsection{Including sampled host information}

Information regarding which host each sample comes from can be included in the following way:

\begin{lstlisting}
 <typeTraitSet spec='TraitSetWithLimits' id='typeTraitSet'   traitname="type"   value="C00013422=PMK1, C00013424=PMK3">
     <taxa spec='TaxonSet' alignment='@alignment'/>
   </typeTraitSet>
\end{lstlisting}

Here "PMK1" and "PMK3" are the names of the hosts, and sample names (here "C00013422" and "C00013424") must be the same as in the alignment (specified here by "alignment").

You must edit this part by entering the sampling location for {\bf{ALL}} your samples.

\subsection{Including sampling times}

Similarly, time data section looks like this:

\begin{lstlisting}
   <timeTraitSet spec='TraitSetWithLimits' id='timeTraitSet'  traitname="date-forward"  value="C00013422=0.5, C00013424=96.5">
     <taxa spec='TaxonSet' alignment='@alignment'/>
   </timeTraitSet>
\end{lstlisting}
   
Which is similar to before, except that now "0.5" and "96.5" are the sampling dates of the two considered taxa.

\subsection{Host introduction and removal times}

Host introduction and removal times can similarly be defined using the following formats respectively:

 \begin{lstlisting}
   <startInfectionsTraitSet spec='InfectionTraitSet' id='startInfectionsTraitSet' type='start'   traitname="date-forward"   value="PMK1=-7, PMK3=84">
 	  <taxaDates idref='timeTraitSet'/>
 	  <taxaTypes idref='typeTraitSet'/>
   </startInfectionsTraitSet>
\end{lstlisting}

and

 \begin{lstlisting}
   <endInfectionsTraitSet spec='InfectionTraitSet' id='endInfectionsTraitSet' type='end'   traitname="date-forward"   value="PMK1=6.9, PMK3=97.9">
 	  <taxaDates idref='timeTraitSet'/>
 	  <taxaTypes idref='typeTraitSet'/>
   </endInfectionsTraitSet>
\end{lstlisting}

These values represent respectively the earliest and latest times at which hosts are allowed to be infected/infectious. For example, in a hospital outbreak, these values might correspond to patient admission and release.


\subsection{Defining the migration model}

the migration model is defined with:

\begin{lstlisting}
   <migrationModelVolz spec='MigrationModelVolz' id='migModel'>
     <rateMatrix spec='RealParameter' value="1.0" dimension="28" id="rateMatrix"/>
     <popSizes spec='RealParameter' value="1.0" dimension="8" id="popSizes"/>
   </migrationModelVolz>
\end{lstlisting}

\begin{lstlisting}
   <migrationModelUniform spec='MigrationModelUniform' id='migModel' minDemes='25'>
     <rate spec='RealParameter' value="0.001" dimension="1" id="rate"/>
     <popSize spec='RealParameter' value="1.0" dimension="1" id="popSize"/>
     <numDemes spec='IntegerParameter' value="27" dimension="1" id="numDemes" lower='25' upper='27'/>
     <trait idref='startInfectionsTraitSet' type='start'/>
     <trait idref='endInfectionsTraitSet' type='end'/>
   </migrationModelUniform>
\end{lstlisting}

Here the most important part are the two number, "25" and "27". the first one is the minimum allowed number of hosts, the second the maximum allowed number of hosts. 
The minimum allowed number of hosts must at least be equal to the number of sampled hosts.
If there are intervals of time in which no sampled host is exposed, then the maximum number of hosts and the starting number of hosts (both 27 here) must be strictly larger than the number of sampled hosts.

You will have to modify the migration model initialisation as well accordingly:

\begin{lstlisting}
<migrationModelUniform spec='MigrationModelUniform' minDemes='25'>
             <rate spec='RealParameter' value="0.001" dimension="1"/>
             <popSize spec='RealParameter' value="1.0" dimension="1"/>
             <numDemes spec='IntegerParameter' value="27" dimension="1"/>
         </migrationModelUniform>
\end{lstlisting}


\subsection{Specifying non-sampled hosts with limited exposure times}

Each non-sampled host in the model by default represents a host with unlimited exposure time, and as such, can effectively represent a large number of real-life hosts.
An alternative in SCOTTI is to define non-sampled host groups as successions of hosts with limited exposure time, such that the removal time of a host corresponds to the introduction time of the next. Different non-sampled host groups have shifted exposure times.
This alternative is generally more computationally demanding and has no appreciable effect on the analysis as long as we have experience of.
If you want to try it, you can simply specify:

\begin{lstlisting}
   <input spec='StructuredCoalescentTreeDensityLimitedLifespan'   id='treePrior'   limitedLifespan='True'>
\end{lstlisting}

in the tree prior section.


\subsection{Specifying location and name of output files}

To specify the file names and location of the output, simply modify the two following lines at the end of the xml file

\begin{lstlisting}
	<logger logEvery="1000" fileName="pathToFile/fileName.log">
\end{lstlisting}

and

\begin{lstlisting}
	 <logger logEvery="10000" fileName="pathToFile/fileName.trees" mode="tree">
\end{lstlisting}

for the log file and the output trees file respectively.


\section{Running SCOTTI}

\subsection{Installing and running SCOTTI using beauti and beast}

Just select the SCOTTI xml file you created, after you double-clicked on the BEAST2 icon in the BEAST2 folder.

\subsection{Running SCOTTI from the command line}

If you want to run SCOTTI from command line instead, first download and unzip from https://bitbucket.org/nicofmay/scotti/src/master/SCOTTI/dist/ the latest version of SCOTTI.
Then, if you are using a mac, place/rename this folder in 
~/Library/Application Support/BEAST/2.7/SCOTTI
Or if you are using Linux, place/rename it here
~/.beast/2.7/SCOTTI


Then follow the instructions in https://www.beast2.org/2019/09/26/command-line-tricks.html

For earlier version of scotti ($\leq 2$) and beast ($\leq 2.5$) the following command could be used:

\begin{lstlisting}
java -cp /SCOTTIfolder/dist/SCOTTI.v1.1.0/lib/SCOTTI.v1.1.0.jar: /SCOTTIfolder/lib/jblas-1.2.3.jar:/SCOTTIfolder/lib/guava-15.0.jar: /BEASTfolder/lib/beast.jar beast.app.beastapp.BeastMain SCOTTI\_XML\_file.xml
\end{lstlisting}

Without spaces after ":", and where:
"SCOTTIfolder" stands for the downloaded SCOTTI package path;
"BEASTfolder" stands for the BEAST2 folder path;
"SCOTTI\_XML\_file.xml" stands for the newly created SCOTTI xml file, including path.


\subsection{Analysing the output}

The output from SCOTTI can be used to get a summary of the inferred transmission events, graph-like plots of hosts and transmissions, and a phylogenetic tree of the samples enriched with host information.
To run these analysis, simply use the python script "Make\_transmission\_tree.py" which can be found within the SCOTTI package or at https://bitbucket.org/nicofmay/scotti/src/ $\rightarrow$ SCOTTI.
 For a description of the formats and options, simply run from terminal "python Make\_transmission\_tree.py -h" from the SCOTTI folder.
 To run it, use the format  "python Make\_transmission\_tree.py -i fileName.trees -o fileNameOutput".
 If you have problems installing Graph-tools, required in the script, you can use the alternative script Make\_transmission\_tree\_alternative.py which should not require additional packages, but which produces less fancy plots.
 To produce phylogenetic trees enriched with host information, you can use treeannotator (part of the BEAST franchise) to summarize the output.trees file into a single tree, and then figTree to visualize the resulting tree. 


\end{document}

